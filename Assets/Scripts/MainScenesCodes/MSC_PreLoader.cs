﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using SM = SceneManager;
using AM = AudioManager;
using QM = QuestionManager;

public class MSC_PreLoader : MainSceneCode {


    IEnumerator InitPreLoader() {
        yield return new WaitForSeconds(2);
        SM.i.FadeIn(() => {
            GameObject.Find("CutSceneObjects").GetComponent<Animator>().speed = 1;
        }, false,false, 2F);
    }



    public void OnJSReceive(string data) {
        Debug.Log("Receive data form JS");
        Debug.Log(data);
        var JD = JSON.Parse(data);
        switch (JD["type"]) {
            case "LESSON_DATA":
                Debug.Log(JD["data"]);
                QM.i.LoadQuestions(JD["data"]);
                break;

        }
    }


    void Start() {
        GameObject.Find("CutSceneObjects").GetComponent<Animator>().SetTrigger("InitIntro");
        StartCoroutine(InitPreLoader());
        AM.i.PlayBGM(BGM_Main);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
