﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainSceneCode : MonoBehaviour {

    public static MainSceneCode i;

    public string output = "";
    public string stack = "";

    //Musics
    public AudioClip BGM_Main;

    void HandleLog(string logString, string stackTrace, LogType type) {
        if (Application.isWebPlayer)
            Application.ExternalCall("OnUnityDebugMessage", logString,stackTrace,type.ToString());
    }

    void OnEnable() {
        Application.logMessageReceived += HandleLog;
    }

    void OnDisable() {
        // Remove callback when object goes out of scope
        Application.logMessageReceived -= HandleLog;
    }

    public T GetGameComponent<T>() {
        var GO = GameObject.Find(typeof(T).ToString());
        if (GO == null) return default(T);
        else return GO.GetComponent<T>();
    }

    public static void CheckGlobals() {
        if (GameObject.Find("GlobalManager") == null) {
            Instantiate(Resources.Load("GlobalManager", typeof(GameObject))).name = "GlobalManager";
        }
        if (GameObject.Find("GlobalCanvas") == null) {
            Instantiate(Resources.Load("GlobalCanvas", typeof(GameObject))).name = "GlobalCanvas";
        }
    }

    public void Awake() {
        i = this;
        CheckGlobals();
    }



}
