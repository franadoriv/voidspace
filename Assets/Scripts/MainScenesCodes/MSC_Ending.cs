﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UnityEngine.UI;
using AM = AudioManager;
using GM = GlobalManager;
using MIF = MIF_Ending;
using SM = SceneManager;
using GCM = GlobalCanvasManager;
using ASM = AudioStreamManager;


public class MSC_Ending : MainSceneCode {

    public static MSC_Ending i;

    public F3DWarpJump WarpTunel;

    public AudioClip SFX_WarpTunel;
    public AudioClip SFX_WarpExplosion;
    public AudioClip SFX_ShipsIncoming;
    public AudioClip SFX_ShipsEngines;
    public AudioClip SFX_Fireworks;

    public Animator ESANMT;

    public GameObject Fireworks;

    public bool CanBeReset = false;
    public bool Reseted = false;

    Coroutine CRT_FwRndSfx;

    IEnumerator FwRndAct(List<Transform> FWL) {
        for(int N = 0; N < FWL.Count; N++) {
            FWL[N].gameObject.SetActive(true);
            yield return new WaitForSeconds(UnityEngine.Random.Range(1.0F,2.0F));
        }
        yield return new WaitForEndOfFrame();
    }
    IEnumerator FwRndSfx() {
        yield return new WaitForSeconds(1);
        while (true) {
            AM.i.PlaySFX(SFX_Fireworks);
            yield return new WaitForSeconds(UnityEngine.Random.Range(1.0F, 2.0F));
        }
    }

    public void C1P1() {
        WarpTunel.gameObject.SetActive(true);
        GM.i.DelayAction(2, () => { AM.i.PlaySFX(SFX_WarpTunel); });
        GM.i.DelayAction(3.5F, () => { AM.i.PlaySFX(SFX_WarpExplosion); });
    }

    public void ActivateFireworks() {
        var FWL = Fireworks.GetComponentsInChildren<Transform>(true).Where(T => T.name == "FX_Firework");
         StartCoroutine(FwRndAct(FWL.OrderBy(a => Guid.NewGuid()).ToList()));
        CRT_FwRndSfx = StartCoroutine(FwRndSfx());
        var SAS = GameObject.Find("SpaceShipFleet2").GetComponent<AudioSource>();
        GM.i.DelayAction(2, () => { iTween.ValueTo(gameObject, 1.0F, 0.0F, 0.5F, (V) => { SAS.volume = (float)V; }); });
    }

    void ResetGame() {
        if (Reseted || !CanBeReset) return;
        Reseted = true;
        StopCoroutine(CRT_FwRndSfx);
        MIF.i.FadeAllTexts();
        SM.i.ChangeScene("IntroGame");
    }

    void Awake() { base.Awake(); i = this; }

	// Use this for initialization
	void Start () {
        ASM.i.Play("BGM_Ending", () => {
            AM.i.PlayBGM(BGM_Main);
            GCM.i.Fade(true);
            ESANMT.enabled = true;
        });
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.anyKeyDown || Input.GetMouseButton(0)) {
            ResetGame();
        }
	}
}
