﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using GM = GlobalManager;
using SM = SceneManager;
using AM = AudioManager;
using UnityStandardAssets.ImageEffects;
using ALTM = AlertManager;
using GCM = GlobalCanvasManager;
using ASM = AudioStreamManager;
using LM = LocalizationManager;

public class MSC_HangarIntro : MainSceneCode {
    public static MSC_HangarIntro i;
    public bool Skiped = false;
    public bool CanBeenSkiped = true;

    public PanelBase PNL_Instructions;
    public AudioClip SFX_OnHover;
    public AudioClip SFX_FinalWarp;

       public void OnOptionHover() {
        AM.i.PlaySFX(SFX_OnHover);
    }

    void BloomAndExit() {
        if (Skiped) return;
        var BO = GameObject.Find("CAM_Cockpit").GetComponent<BloomOptimized>();
        AM.i.PlaySFX(SFX_FinalWarp);
        iTween.ValueTo(gameObject, BO.intensity, 30.0F, 2.0F, (V) => {
            BO.intensity = (float)V;
        }, () => {
            SM.i.ChangeScene("MainGame");
        });
    }

    void Skip() {
        Skiped = true;
        CanBeenSkiped = false;
        SM.i.ChangeScene("MainGame");
    }

    public void IntroFinished() {
        if (Skiped) return;
        LM.i.GetText("TXT_Q_GAME_PLAY_INSTRUCTIONS",
                                      "Do you wanna read Gameplay instructions?", 
                                      (STR) => {
                                          ALTM.i.Display(STR,
                                              () => {
                                                  PNL_Instructions.gameObject.SetActive(true);
                                                  PNL_Instructions.Show();
                                              }, BloomAndExit);
                                      });

    }

    public void CloseInstructions() {
        PNL_Instructions.Show(false, () => { BloomAndExit() ; });
    }

    // Use this for initialization
    void Awake () {
        base.Awake();
        i = this;
	}

    void Start() {
        AM.i.PlayBGM(BGM_Main);
        ASM.i.Play("BGM_HangarIntro", () => {
            AM.i.SetVolume(1.0F);
            PlayerHelper Player = GameObject.Find("Player").GetComponent<PlayerHelper>();
            GM.i.DelayAction(3.0F, () => {
                GCM.i.Fade(true);
            });
        });
    }
	

	// Update is called once per frame
	void Update () {
        if (CanBeenSkiped && Input.anyKeyDown)
            Skip();
	}
}
