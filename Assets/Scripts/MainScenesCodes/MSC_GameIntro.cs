﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;
using System.Collections.Generic;
using GM = GlobalManager;
using AM = AudioManager;
using GCM = GlobalCanvasManager;
using SM = SceneManager;
using QM = QuestionManager;
using LM = LocalizationManager;

public class MSC_GameIntro : MainSceneCode {

    public static MSC_GameIntro i;

    public Image IMG_FadeScreen;
    public Animator GameIntroAnimator;
    public GameObject SpaceShipFleet;
    public Image IMG_GameTitle;
    public Text LBL_PressAnyKey;
    public Text LBL_WorkInProgress;
    bool IntroFinished = false;
    public AudioSource SFXL_MotherShip;


    IEnumerator FleetMovement() {
        while (true) {
            SpaceShipFleet.transform.Translate(new Vector3(0,0,0.02F));
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator LoopFade(MaskableGraphic g,float TimeSeconds = 2.5F) {
        while (true) {
            g.CrossFadeAlpha(0, TimeSeconds/2, true);
            while (g.GetComponentsInChildren<CanvasRenderer>()[0].GetAlpha() > 0.0F) {
                yield return new WaitForSeconds(1);
            }
            g.CrossFadeAlpha(1, TimeSeconds / 2, true);
            while (g.GetComponentsInChildren<CanvasRenderer>()[0].GetAlpha() < 1.0F) {
                yield return new WaitForSeconds(1);
            }
            yield return new WaitForSeconds(1);
        }
    }

    public void ListenMotherShip() { 
        iTween.ValueTo(SFXL_MotherShip.gameObject,0.0F,1.0F,2.0F,(V)=>{
            SFXL_MotherShip.volume = (float)V;
        });
        
    }

    public void FinishIntro() {
        if (!IntroFinished) {
            if(PressAnyKeyBounce!=null)
                StopCoroutine(PressAnyKeyBounce);
            IntroFinished = true;
            AudioClip AC = GameObject.Find("SoundLibraryHelper").GetComponent<SoundLibraryHelper>().Sounds.Where(W => W.name == "engine_warp_002").FirstOrDefault();
            AM.i.PlaySFX(AC);
            IMG_GameTitle.CrossFadeAlpha(0.0F, 3, true);
            LBL_PressAnyKey.CrossFadeAlpha(0.0F, 3, true);
            IMG_FadeScreen.CrossFadeAlpha(1.0F, 5, true);
            GM.i.DelayAction(2,()=> { SM.i.ChangeScene("IntroHangar",ForceInitialValue:false); });
        }
    }

    Coroutine PressAnyKeyBounce;
    public void ContinueFleetMovement() {
        Debug.Log("ContinueFleetMovement");
        GameIntroAnimator.StopPlayback();
        StartCoroutine(FleetMovement());
        IMG_GameTitle.CrossFadeAlpha(1,5, false);
        GM.i.DelayAction(5.0F, () => {
            PressAnyKeyBounce = StartCoroutine(LoopFade(LBL_PressAnyKey));
        });
    }

    public void Init() {
        //GM.i.DelayAction(3.0F, () => { LBL_WorkInProgress.CrossFadeAlpha(1.0F, 5.0F, true); });
        //GM.i.DelayAction(10.0F, () => { LBL_WorkInProgress.CrossFadeAlpha(0.0F, 5.0F, true); });
        GM.i.DelayAction(5.0F, () => {
            //AM.i.PlayBGM(BGM_Main);
            AudioStreamManager.i.WaitGetSFX("SFX_LoopShipAmbient", (AC) => {
                SFXL_MotherShip.clip = AC;
                SFXL_MotherShip.Play();
            });
            //SFXL_MotherShip.clip = AudioStreamManager.i.GetSFXLoaded("SFX_LoopShipAmbient");
            AudioStreamManager.i.Play("BGM_Intro", () => {
                GameIntroAnimator.enabled = true;
                GCM.i.Fade(true);
            });
        });
        //GM.i.DelayAction(18.0F, () => {GameIntroAnimator.enabled = true;});
        //GM.i.DelayAction(19.0F, () => { GCM.i.Fade(true); });
    }

    void Awake() {
        base.Awake();
        i = this;
        LBL_PressAnyKey.CrossFadeAlpha(0f, 0f, false);
        IMG_GameTitle.CrossFadeAlpha(0f, 0f, false);
        LBL_WorkInProgress.CrossFadeAlpha(0f, 0f, false);
    }

	// Use this for initialization
	void Start () {
        Debug.Log(GM.i);
        if (!QM.i.QuestionaryLoaded) {
            QM.i.Load(Init,
                (ErrMess) => {
                    //LBL_Loading.gameObject.SetActive(false);
                    //ALTH.ShowAlertBox(ErrMess, "Error");
                });
        }
        else
            Init();
        LM.i.FillLabel("PRESS_ANY_KEY", LBL_PressAnyKey, "PRESS ANY KEY");
    }


	
	// Update is called once per frame
	void Update () {
	
	}
}
