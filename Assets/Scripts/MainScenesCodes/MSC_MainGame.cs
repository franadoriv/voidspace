﻿using UnityEngine;
using System.Collections;
using GCM = GlobalCanvasManager;
using GM = GlobalManager;
using QM = QuestionManager;
public class MSC_MainGame : MainSceneCode {

    public static MSC_MainGame i;

    public PositionMatrix PM;

    public AudioClip BGM_GameOver;

    // Use this for initialization
    void Awake() {
        i = this;
        base.Awake();
    }

    void Start() {
        GCM.i.TEX_ScreenFader.color = new Color(1, 1, 1, 1);
        PM = PositionMatrix.i;
        PlayerHelper.i.ChangeView(PlayerHelper.ViewTypes.cocktip, () => {
            MGL_MainGame.i.InitIntro();
            //GM.i.DelayAction(1, () => { MGL_MainGame.i.InitIntro(); });
        });
        //Just in case is not loaded from the first scene
        QM.i.Load();
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
