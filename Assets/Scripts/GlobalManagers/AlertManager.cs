﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using UnityEngine.UI;
using AM = AudioManager;
using LM = LocalizationManager;

public class AlertManager : PanelBase {
    public static AlertManager i ;

    public Text LBL_Message;
    public Text LBL_YES;
    public Text LBL_NO;

    public Action OnAfirmative, OnNegative;
    public AudioClip SFX_OnClick;
    public AudioClip SFX_OnHover;

    public void Answer(GameObject BTN) {
        AM.i.PlaySFX(SFX_OnClick);
        Debug.Log("Answer -> " + BTN.name);
        Show(false, () => {
            if (BTN.name.Contains("YES") && OnAfirmative != null)
                OnAfirmative();
            else if (BTN.name.Contains("NO") && OnNegative != null)
                OnNegative();
        });
    }

    public void OnOptionHover() {
        AM.i.PlaySFX(SFX_OnHover);
    }

    public void Display(string Text, Action OnAfirmative = null, Action OnNegative = null) {
        LBL_Message.text = Text;
        this.OnAfirmative = OnAfirmative;
        this.OnNegative = OnNegative;
        Show();
    }


    void Awake() {
        base.Awake();
        i = this;
        gameObject.SetActive(false);
    }

    // Use this for initialization
    void Start() {
        LM.i.FillLabel("LBL_YES", LBL_YES, "YES");
        LM.i.FillLabel("LBL_NO", LBL_NO, "NO");
    }

    // Update is called once per frame
    void Update() {

    }
}
