﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioStreamManager : MonoBehaviour {

    public static AudioStreamManager i;
    public static List<StreamedAudio> StreamList = new List<StreamedAudio>();

    public class StreamedAudio {
        public int ID = 0;
        public string Name = "";
        public string URL = "";
        public AudioClip clip;

        public static StreamedAudio Find(int ID) { return AudioStreamManager.StreamList.Find(SA => SA.ID == ID); }
        public static StreamedAudio Find(string Name) { return AudioStreamManager.StreamList.Find(SA => SA.Name == Name); }

        public static IEnumerator StreamLoad(int ID,int Delay ,Action OnLoad = null, Action<string> OnError = null) {
            var SA = StreamList.Find(SAL => SAL.ID == ID);
            SA.LoadStatus = StreamedAudio.LoadStates.Loading;
            yield return new WaitForSeconds(Delay);
            var Loader = new WWW(StreamedAudio.Find(ID).URL);
            Debug.Log("Loading " + StreamedAudio.Find(ID).Name);
            while (!Loader.isDone)
                yield return new WaitForEndOfFrame();
            AudioClip clip = Loader.GetAudioClip(false, false, Application.isEditor?AudioType.OGGVORBIS:AudioType.MPEG);
            if (!String.IsNullOrEmpty(Loader.error)) {
                Debug.Log("Loading error for " + StreamedAudio.Find(ID).Name + ": " + Loader.error);
                StreamedAudio.Find(ID).LoadStatus = StreamedAudio.LoadStates.Fail;
                Debug.Log(Loader.error);
                if (OnError != null) OnError(Loader.error);
            }
            else {
                if (clip == null) { 
                    Debug.Log("Sorry, www.audioClip is null");
                    StreamedAudio.Find(ID).LoadStatus = StreamedAudio.LoadStates.Fail;
                    if (OnError != null) OnError("AudioClip is null");
                }
                else {
                    Debug.Log("Wating  " + StreamedAudio.Find(ID).Name + " to be ready for play");
                    while (!clip.isReadyToPlay)
                        yield return new WaitForSeconds(1);
                    Debug.Log(StreamedAudio.Find(ID).Name + " ITS READY FOR PLAY BABY!!!");
                    StreamedAudio.Find(ID).clip = clip;
                    StreamedAudio.Find(ID).LoadStatus = StreamedAudio.LoadStates.Loaded;
                    if (OnLoad != null)
                        OnLoad();
                }
            }
        }

        public static IEnumerator WaitAndPlay(StreamedAudio SA, Action OnPlay) {
            if (SA.LoadStatus != StreamedAudio.LoadStates.Loaded)
                Debug.Log("Waiting for play " + SA.Name);
            while (SA.LoadStatus != StreamedAudio.LoadStates.Loaded && SA.LoadStatus != StreamedAudio.LoadStates.Fail)
                yield return null;
            if (SA.LoadStatus == StreamedAudio.LoadStates.Loaded) {
                Debug.Log(SA + " load ready! now playing");
                AudioManager.i.PlayBGM(SA.clip);
                if (OnPlay != null)
                    OnPlay();
            }
            else {
                Debug.Log("Looad fail!!");
            }
        }


        public LoadStates LoadStatus = LoadStates.Waiting;
        public enum LoadStates { Waiting,Loading,Loaded,Fail}
  
        public StreamedAudio(string Name,string URL,bool LoadOnce = true,int Delay = 0,Action OnLoad = null) {
            StreamList.Add(this);
            this.Name = Name;
            this.ID = StreamList.Count+1;
            this.URL = URL;
            if(LoadOnce)
            i.StartCoroutine(StreamedAudio.StreamLoad(this.ID, Delay,OnLoad: OnLoad));
            
        }

    }

    public void Play(string Name,Action OnStart) {
        var BGM = StreamedAudio.Find(Name);
        if (BGM != null)
            StartCoroutine(StreamedAudio.WaitAndPlay(BGM, OnStart));
    }

    public void PlaySFX(string Name,AudioSource source = null) {
        var SFX = StreamedAudio.Find(Name);
        if (SFX != null)
            if (SFX.clip != null)
                if (SFX.clip.isReadyToPlay)
                    if (source != null)
                        source.PlayOneShot(SFX.clip);
                    else
                        AudioManager.i.PlaySFX(SFX.clip);
    }

    public AudioClip GetSFXLoaded(string Name) {
        AudioClip R = null;
        var SFX = StreamedAudio.Find(Name);
        if (SFX != null) {
            if (SFX.clip != null)
                if (SFX.clip.isReadyToPlay)
                    R = SFX.clip;
        }
        //Debug.Log("GetSFXLoaded failed because " + Name + " its not ready");
        return R;
    }

    IEnumerator WaitForLoadSFX(string Name,Action<AudioClip> OnLoad)  {
        while (GetSFXLoaded(Name) == null) {
            yield return new WaitForSeconds(1);
        }
        OnLoad(GetSFXLoaded(Name));
    }

    public void WaitGetSFX(string Name,Action<AudioClip> OnLoad) {
        StartCoroutine(WaitForLoadSFX(Name, OnLoad));
    }

    void Awake() {//
        i = this;
        if (Application.isEditor) {
            new StreamedAudio("BGM_Intro", "https://dl.dropboxusercontent.com/s/f6kd2rhmllmedo2/BGM_Intro.ogg");
            //new StreamedAudio("SFX_LoopShipAmbient", "https://dl.dropboxusercontent.com/s/di6vpw4q5lropsw/SFX_LoopShipAmbient.mp3");
            new StreamedAudio("BGM_HangarIntro", "https://dl.dropboxusercontent.com/s/hiaiiacmhcqjup1/BGM_HangarIntro.ogg", Delay: 2);
            new StreamedAudio("BGM_InGame", "https://dl.dropboxusercontent.com/s/dgmjah6n0sniujk/BGM_Ingame.ogg", Delay: 4);
            //new StreamedAudio("SFX_pulsating_tone", "https://dl.dropboxusercontent.com/s/njhia3hx4ic3uj3/SFX_pulsating_tone.mp3", Delay: 5);
            new StreamedAudio("BGM_GameOver", "https://dl.dropboxusercontent.com/s/xh2z31tyf8vgo4w/BGM_GameOver.ogg", Delay: 10);
            new StreamedAudio("BGM_Ending", "https://dl.dropboxusercontent.com/s/6y3nvgvh7akv265/BGM_Ending.ogg", Delay: 20);
        }
        else {
            new StreamedAudio("BGM_Intro", "https://dl.dropboxusercontent.com/s/d5pi3mcgsctqwgo/BGM_Intro.mp3");
            new StreamedAudio("SFX_LoopShipAmbient", "https://dl.dropboxusercontent.com/s/di6vpw4q5lropsw/SFX_LoopShipAmbient.mp3");
            new StreamedAudio("BGM_HangarIntro", "https://dl.dropboxusercontent.com/s/nlfai1b1lszanhv/BGM_HangarIntro.mp3", Delay: 2);
            new StreamedAudio("BGM_InGame", "https://dl.dropboxusercontent.com/s/sbvcucvci10hc16/BGM_Ingame.mp3", Delay: 4);
            new StreamedAudio("SFX_pulsating_tone", "https://dl.dropboxusercontent.com/s/njhia3hx4ic3uj3/SFX_pulsating_tone.mp3", Delay: 5);
            new StreamedAudio("BGM_GameOver", "https://dl.dropboxusercontent.com/s/vs99v53lqql4fo3/BGM_GameOver.mp3", Delay: 10);
            new StreamedAudio("BGM_Ending", "https://dl.dropboxusercontent.com/s/54yy3irob4qsdzj/BGM_Ending.mp3", Delay: 20);
        }
    }

	// Use this for initialization
	void Start () {
       

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
