﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Linq.Expressions;
using SimpleJSON;
using GM = GlobalManager;
using MIF = MIF_MainGame;
using RestSharp.Contrib;
//using RestSharp.Contrib;

public class QuestionManager : MonoBehaviour {
    public static QuestionManager i;
    int MaxQuestions = 100;
    public bool QuestionaryLoaded = false;
    public bool LoadFromWeb = true;
    public bool FirstLoad = false;
    public static string FilesFolders = GlobalVariables.ConfigValues.FilesFolders;
    public static string QuestionsFolder = GlobalVariables.ConfigValues.QuestionsFolder;
    public static string QuestionaryToken = "14422658569652053789196";

    public List<Question> Questionary = new List<Question>();
    public List<Question> Quested = new List<Question>();
    public Question CurrentQuestion;
    public int CurrentAnswerNumber = 0;
    public bool ConfirmationPhase = false;

    public UIPanel PNL_QuestionAndAnswers;

    Hashtable AnswersAngles = new Hashtable();

    public class MediaResource {
        public string LoadError;
        public DataType MediaType;
        public Texture2D Image;
        public AudioClip Audio;
#if UNITY_ANDROID
        public MobileMovieTexture Video;
#endif
#if UNITY_WEBPLAYER
            public MovieTexture Video;
#endif

        public string MediaPath;
    }

    public class Question {
        public int Number = 0;
        public DataType QuestionType;
        public string value = "";
        public List<Answer> Answers = new List<Answer>();
        public MediaResource Media = new MediaResource();
        public bool MediaReady = false;
        public class Answer {
            public string value;
            public DataType AnswerType;
            public bool iscorrect;
            public int ordinal;
            public char letter;
            public MediaResource Media = new MediaResource();
            public bool MediaReady = false;
            public Answer(string value, DataType AnswerType, bool iscorrect, int ordinal) {
                this.value = value;
                this.AnswerType = AnswerType;
                this.iscorrect = iscorrect;
                this.ordinal = ordinal;
                switch (ordinal) {
                    case 0: this.letter = 'A'; break;
                    case 1: this.letter = 'B'; break;
                    case 2: this.letter = 'C'; break;
                    case 3: this.letter = 'D'; break;
                    case 4: this.letter = 'E'; break;
                    case 5: this.letter = 'F'; break;
                }
                if (AnswerType != DataType.Text) {
                    this.Media.MediaType = AnswerType;
                    this.value = this.value.Replace("/Memarden/testResources/", "");
                    this.Media.MediaPath = FilesFolders + this.value;
                    Debug.Log("A -> FilesFolder: " + FilesFolders + " this.value: " + this.value + " MediaPath: " + this.Media.MediaPath);
                    //this.Media.MediaPath = FilesFolders.Contains("Memarden/testResources") ? this.value : FilesFolders + (this.value);
                }
            }
        }
        public Question(int Number, DataType QuestionType, string value) {
            this.Number = Number;
            this.QuestionType = QuestionType;
            this.value = value;
            this.Media.MediaType = QuestionType;
            if (QuestionType != DataType.Text) {
                this.Media.MediaType = QuestionType;
                this.value = this.value.Replace("/Memarden/testResources/", "");
                this.Media.MediaPath = FilesFolders + this.value;
                Debug.Log("Q -> FilesFolder: " + FilesFolders + " this.value: " + this.value + " MediaPath: " + this.Media.MediaPath);
            }
        }

    }

    public enum DataType { Text, Image, Sound, Video, Unknow }

    public DataType GetDataType(string DataTypeName) {
        //Debug.Log("GetDataType: " + DataTypeName);
        switch (DataTypeName) {
            case "Text": return DataType.Text;
            case "Image": return DataType.Image;
            case "Sound": return DataType.Sound;
            case "Video": return DataType.Video;
        }
        return DataType.Unknow;
    }

    public void OnGenericError(string ErrorMessage, Action<string> OnGenericErrorFinish = null) {
        string ErrStr = "Error on Question Manager: " + ErrorMessage;
        Debug.Log(ErrStr);
        if (OnGenericErrorFinish != null)
            OnGenericErrorFinish(ErrStr);

    }

    //public Action<string> OnErrorListener;

    IEnumerator DownloadText(string URL, Dictionary<string, string> Parameters = null, Action<string> OnFinish = null, Action<string> OnError = null) {
        URL = URL.Replace("%00", "");
        Debug.Log("Downloading text from: " + URL);
        WWW www;
        byte[] rawData = null;
        if (Parameters != null) {
            foreach (var Parameter in Parameters) {
                Debug.Log("Parameter " + Parameter.Key + " = " + Parameter.Value);
            }
            WWWForm form = new WWWForm();
            foreach (var Parameter in Parameters)
                form.AddField(Parameter.Key, Parameter.Value);
            rawData = form.data;
            www = new WWW(URL, rawData);
        }
        else
            www = new WWW(URL);
        yield return www;
        //foreach (var H in www.responseHeaders) Debug.Log(H.Key + ": " + H.Value);
        Debug.Log("Data recived: " + (String.IsNullOrEmpty(www.error) ? www.text : www.error));

        if (!string.IsNullOrEmpty(www.error))
            OnGenericError(www.error, OnError);
        else if (OnFinish != null)
            OnFinish(www.text);
    }

    IEnumerator LoadMedia(MediaResource MR, Action<MediaResource, bool> OnFinish, Action<string> OnError = null) {
        bool MediaReady = false;
        if (MR.MediaType == DataType.Image || MR.MediaType == DataType.Sound || MR.MediaType == DataType.Video) {
            //The memarden Framework doesnt return extensions
            /*switch (MR.MediaType) {
                case DataType.Image: MR.MediaPath += MR.MediaPath.Contains(".jpg") ? "" : ".jpg"; break;
                case DataType.Sound: MR.MediaPath += MR.MediaPath.Contains(".ogg") ? "" : ".ogg"; break;
                case DataType.Video: MR.MediaPath += MR.MediaPath.Contains(".ogv") ? "" : ".ogv"; break;
            }*/

#if UNITY_ANDROID
            if (MR.MediaType == DataType.Video) {
                MR.Video.Path = MR.MediaPath;
            }
#endif
            WWW www = null;
            if (MR.MediaType != DataType.Video || !Application.isMobilePlatform) {
                Debug.Log("Downloading media from: " + MR.MediaPath);
                www = new WWW(MR.MediaPath);
                yield return www;
                MR.LoadError = www.error;
            }
            if (!string.IsNullOrEmpty(MR.LoadError)) OnGenericError(MR.LoadError, OnError);
            else {
                Debug.Log("Loaded media type: " + MR.MediaType);
                switch (MR.MediaType) {
                    case DataType.Image: MR.Image = www.texture; break;
                    case DataType.Sound:
                        MR.Audio = www.GetAudioClip();
                        while (MR.Audio.loadState != AudioDataLoadState.Loaded) yield return new WaitForEndOfFrame();
                        Debug.Log("Audio is ready");
                        break;
                    case DataType.Video:
#if UNITY_WEBPLAYER
                            var movieTexture = www.movie;
                            while (!movieTexture.isReadyToPlay) yield return new WaitForEndOfFrame();
                            Debug.Log("Video is ready");
                            MR.Video = www.movie;
#endif
                        break;
                }
                MediaReady = true;
            }
        }
        else
            MediaReady = true;
        if (OnFinish != null) OnFinish(MR, MediaReady);
    }

    IEnumerator InitMAPI(Action OnFinish = null) {
        Debug.Log("Loading jquery-2.0.0.min.js");
        Application.ExternalEval("var js1 = document.createElement('script');js1.type = 'text/javascript';js1.src = '../../../../commonJS/jquery-2.0.0.min.js';document.body.appendChild(js1);");
        yield return new WaitForSeconds(1.0F);
        Debug.Log("Loading publicFunctions");
        Application.ExternalEval("var js2 = document.createElement('script');js2.type = 'text/javascript';js2.src = '../../../../commonJS/publicFunctions.js';document.body.appendChild(js2);");
        yield return new WaitForSeconds(1.0F);
        Debug.Log("Loading canvasFunctions");
        Application.ExternalEval("var js3 = document.createElement('script');js3.type = 'text/javascript';js3.src = '../../../../commonJS/canvasFunctions.js';document.body.appendChild(js3);");
        yield return new WaitForSeconds(2.0F);
        Debug.Log("Loading questionary from jsapi");
        string script = "console.log(map_getLessonDataAsString());SendMessage('QuestionManager', 'OnLessonData', map_getLessonDataAsString());";
        yield return new WaitForSeconds(1.0F);
        Debug.Log("Executing: " + script);
        Application.ExternalEval(script);
        yield return new WaitForSeconds(1.0F);
        Debug.Log("Finishing");
        if (OnFinish != null)
            OnFinish();
    }

    public Question GetRandQuestion() {
        Question QS = Questionary.Find(Q => !Quested.Contains(Q));
        if (QS != null) {
            Quested.Add(QS);
        }
        return QS;
    }

    public Question NextQuestion() {
        if (CurrentQuestion.Number < Questionary.Count - 1) {
            CurrentQuestion = Questionary[CurrentQuestion.Number + 1];
            return CurrentQuestion;
        }
        else return null;
    }

    public void ResetQuestionary() {
        CurrentQuestion = Questionary[0];
    }

    public void LoadQuestionsTestFile(Action<List<Question>> OnFinish = null, Action<string> OnError = null) {
        LoadQuestionsFile("lessonData4.txt", true, OnFinish, OnError);
    }

    public void LoadQuestionsTestData(Action<List<Question>> OnFinish = null, Action<string> OnError = null) {
        TextAsset txt = (TextAsset)Resources.Load("Q2", typeof(TextAsset));
        LoadQuestions(txt.text, OnFinish, OnError);
    }


    public void LoadQuestionsFile(string FileName, bool UseDefaultFolder = false, Action<List<Question>> OnFinish = null, Action<string> OnError = null) {
        StartCoroutine(DownloadText((UseDefaultFolder ? QuestionsFolder : "") + FileName, null, (T) => { LoadQuestions(T, OnFinish, OnError); }));
    }

    void GetLessonData() {
        Debug.Log("Application.isWebPlayer: " + Application.isWebPlayer);
        if (Application.isWebPlayer) {
            Debug.Log("Sending GET_LESSON request");
            Application.ExternalCall("OnUnitySend", "GET_LESSON");
        }
        else
            LoadQuestionsTestData();
    }

    public void LoadQuestions(string QuestionsFileText, Action<List<Question>> OnFinish = null, Action<string> OnError = null) {
        /*Application.RegisterLogCallback((string logString, string stackTrace, LogType type) => {
            if (type == LogType.Exception || type == LogType.Error)
            OnError(logString);
        });*/

        try {
            Debug.Log("Parsing JSon File");
            Debug.Log(QuestionsFileText);
            var JD = JSON.Parse(QuestionsFileText);
            Debug.Log("JSon pase OK! " + JD["questions"].Count + " questions found");
            int N = 0;
            Questionary.Clear();
            foreach (JSONNode QJN in JD["questions"].AsArray) {
                Question Q = new Question(N, GetDataType(QJN["question"]["type"]), QJN["question"]["value"].Value);
                foreach (JSONNode AJN in QJN["answers"].AsArray) {
                    var A = new Question.Answer(AJN["value"], GetDataType(AJN["type"]), AJN["isCorrect"].Value.Contains("true"), AJN["ordinal"].AsInt);
                    StartCoroutine(LoadMedia(A.Media, (MR, MediaReady) => { A.Media = MR; A.MediaReady = MediaReady; }));
                    Q.Answers.Add(A);
                }
                StartCoroutine(LoadMedia(Q.Media, (MR, MediaReady) => { Q.Media = MR; Q.MediaReady = MediaReady; }));
                Questionary.Add(Q);
                N++;
                if (N >= MaxQuestions) break;
            }
            CurrentQuestion = Questionary[0];
            if (OnFinish != null)
                OnFinish(Questionary);
        }
        catch (Exception e) {
            OnGenericError(e.Message, OnError);
        }
    }

    public void LoadQuestionAnswersPanel(int QuestionIndex) {
        var MSC = GameObject.Find("MainSceneCode").GetComponent<MainSceneCode>();
        Question Q = Questionary[QuestionIndex];
        Transform QC = GM.i.SearchTransform(GM.i.transform, "CNT_Question");
        Transform[] ACS = GM.i.SearchTransforms(GM.i.transform, "BTN_Answer_", false);

        //Fill Question
        GM.i.SearchTransform(QC.transform, "LBL_Text").gameObject.SetActive(Q.QuestionType == DataType.Text);
        GM.i.SearchTransform(QC.transform, "TEX_Image").gameObject.SetActive(Q.QuestionType == DataType.Image || Q.QuestionType == DataType.Video || Q.QuestionType == DataType.Sound);
        switch (Q.QuestionType) {
            case DataType.Text:
                GM.i.SearchTransform(QC.transform, "LBL_Text").GetComponent<UILabel>().text = Q.value;
                break;
            case DataType.Image:
                GM.i.SearchTransform(QC.transform, "TEX_Image").GetComponent<UITexture>().mainTexture = Q.Media.Image;
                break;
            case DataType.Video:
                //MSC.GM.SearchTransform(QC.transform, "TEX_Image").GetComponent<UITexture>().mainTexture = Q.Media.Video;
                break;
        }

        //Fill answers
        for (int N = 0; N < 6; N++) {
            ACS[N].gameObject.SetActive(N < Q.Answers.Count);
            if (N < Q.Answers.Count) {
                GM.i.SearchTransform(ACS[N].transform, "LBL_Text").gameObject.SetActive(Q.Answers[N].AnswerType == DataType.Text);
                GM.i.SearchTransform(ACS[N].transform, "TEX_Image").gameObject.SetActive(Q.Answers[N].AnswerType == DataType.Image || Q.Answers[N].AnswerType == DataType.Video || Q.Answers[N].AnswerType == DataType.Sound);
                switch (Q.Answers[N].AnswerType) {
                    case DataType.Text:
                        GM.i.SearchTransform(ACS[N].transform, "LBL_Text").GetComponent<UILabel>().text = Q.Answers[N].value;
                        break;
                    case DataType.Image:
                        GM.i.SearchTransform(ACS[N].transform, "TEX_Image").GetComponent<UITexture>().mainTexture = Q.Answers[N].Media.Image;
                        break;
                    case DataType.Video:
                        //MSC.GM.SearchTransform(ACS[N].transform, "TEX_Image").GetComponent<UITexture>().mainTexture = Q.Answers[N].Media.Video;
                        break;
                }
            }
        }

    }

    public bool ExistMediaContent() {
        return Questionary.Any(Q =>
                            Q.QuestionType == QuestionManager.DataType.Sound ||
                            Q.QuestionType == QuestionManager.DataType.Video ||
                            (Q.Answers.Any(A =>
                            A.AnswerType == QuestionManager.DataType.Sound ||
                            A.AnswerType == QuestionManager.DataType.Video))
                            );
    }


    Action<List<Question>> OnLessonDataFinish;
    Action<string> OnLessonDataError;
    public void OnLessonData(string JSonStr) {
        Debug.Log("Lesson data received from exterior: " + JSonStr);
        LoadQuestions(JSonStr, OnLessonDataFinish, OnLessonDataError);
    }

    //****** PRINCIPAL LOAD QUESTIONS FUNCTIONS ********//


    public void Load(Action OnFinish = null, Action<string> OnError = null) {
        FirstLoad = true;
        Action<List<Question>> OnLoadFinish = (QS) => {
            QuestionaryLoaded = true;
            if (OnFinish != null)
                OnFinish();
        };
        Debug.Log("QuestionsOrigin: " + GlobalVariables.ConfigValues.QuestionsOrigin.ToString());
        switch (GlobalVariables.ConfigValues.QuestionsOrigin) {
            case GlobalVariables.QuestionsOrigins.INTERNAL_FILE:
                LoadQInternalFile(OnLoadFinish, OnError);
                break;
            case GlobalVariables.QuestionsOrigins.EXTERNAL_FILE:
                LoadQExternalFile(OnLoadFinish, OnError);
                break;
            case GlobalVariables.QuestionsOrigins.EXTERNAL_REQUEST:
                LoadQExternalRequest(OnLoadFinish, OnError);
                break;
            case GlobalVariables.QuestionsOrigins.LOCAL_MAPI:
                LoadQLocalMApi(OnLoadFinish, OnError);
                break;
        }

    }

    public void LoadQInternalFile(Action<List<Question>> OnFinish = null, Action<string> OnError = null) {
        TextAsset txt = (TextAsset)Resources.Load(GlobalVariables.ConfigValues.TestDataFile, typeof(TextAsset));
        LoadQuestions(txt.text, OnFinish, OnError);
    }

    public void LoadQExternalFile(Action<List<Question>> OnFinish = null, Action<string> OnError = null) {
        TextAsset txt = (TextAsset)Resources.Load(GlobalVariables.ConfigValues.TestDataFile, typeof(TextAsset));
        LoadQuestions(txt.text, OnFinish, OnError);
    }

    public void LoadQExternalRequest(Action<List<Question>> OnFinish = null, Action<string> OnError = null) {
        var Parameters = new Dictionary<string, string>();
        Parameters.Add("URL", GlobalVariables.ConfigValues.MemardenExternalApiUrl.Replace("TOKEN_NUMBER", QuestionaryToken));
        string URL = GlobalVariables.ConfigValues.QueryBridge + @"Get?URL=" + HttpUtility.UrlEncode(GlobalVariables.ConfigValues.MemardenExternalApiUrl.Replace("TOKEN_NUMBER", QuestionaryToken));
        Debug.Log("Searching questionary using: " + URL);
        StartCoroutine(DownloadText(URL, null, (T) => { LoadQuestions(T, OnFinish, OnError); }, OnError));
    }

    public void LoadQLocalMApi(Action<List<Question>> OnFinish = null, Action<string> OnError = null) {
        OnLessonDataFinish = OnFinish;
        OnLessonDataError = OnError;
        StartCoroutine(InitMAPI());
    }

    // Use this for initialization
    void Awake() {
        i = this;
        Debug.Log("Question Manager init");
        Debug.Log("Application.platform: " + Application.platform.ToString());
        Debug.Log("Application.isWebPlayer: " + Application.isWebPlayer);
        if (Application.isWebPlayer || LoadFromWeb || Application.platform.ToString() == "WebGLPlayer") {
            Debug.Log("Yeah baby isWebPlayer");
            Debug.Log("Application.absoluteURL: " + Application.absoluteURL);

            var uri = new Uri(Application.absoluteURL);
            string host = uri.Host;
            string queryString = uri.Query;
            var Parameters = HttpUtility.ParseQueryString(Application.absoluteURL);
            string token = "";

            if (GlobalVariables.ConfigValues.QuestionsOrigin == GlobalVariables.QuestionsOrigins.LOCAL_MAPI) {
                FilesFolders = "../../../../" + FilesFolders;
                QuestionsFolder = "../../../../" + QuestionsFolder;
            }
            else {
                if (Application.absoluteURL.Contains("token=")) {
                    token = Application.absoluteURL.Split('?')[1].Split('=')[1];
                }
                if (Parameters["token"] != null || !string.IsNullOrEmpty(token)) {
                    token = Parameters["token"] != null ? Parameters["token"] : token;
                    Debug.Log("Token found: " + token);
                    GlobalVariables.ConfigValues.QuestionsOrigin = GlobalVariables.QuestionsOrigins.EXTERNAL_REQUEST;
                    QuestionaryToken = token;
                    FilesFolders = "http://" + host + "/Memarden/" + FilesFolders;
                    QuestionsFolder = "http://" + host + "/Memarden/" + QuestionsFolder;
                }
                else {
                    Debug.Log("No token parameter found on the URL");
                    GlobalVariables.ConfigValues.QuestionsOrigin = GlobalVariables.QuestionsOrigins.INTERNAL_FILE;
                }
                FilesFolders = "http://" + host + "/Memarden/" + FilesFolders;
                QuestionsFolder = "http://" + host + "/Memarden/" + QuestionsFolder;
            }
        }
        else {
            Debug.Log("Application is not in WebPlayer");
            GlobalVariables.ConfigValues.QuestionsOrigin = GlobalVariables.QuestionsOrigins.EXTERNAL_FILE;
            QuestionaryToken = "1441725383917810480772";
            FilesFolders = "http://localhost/" + FilesFolders;
            QuestionsFolder = "http://localhost/" + QuestionsFolder;
        }
        Debug.Log("GlobalVariables.ConfigValues.QuestionsOrigin :" + GlobalVariables.ConfigValues.QuestionsOrigin.ToString());
    }

    // Update is called once per frame
    void Update() {

    }
}