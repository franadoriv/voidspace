﻿using UnityEngine;
using System.Collections;
using System.Linq;
using AM = AudioManager;

public class EffectManager : MonoBehaviour {

    public MainSceneCode MSC;
    public GameObject[] SlashEffectsPrefabs;
    public AudioClip[] SoundEffects;

    public void PlaySlashEffectIndex(int Index, Vector3 Pos,Transform Parent=null, Quaternion Rot = new Quaternion(), string AudioEffectName = "") {
        var EffectGuide = SlashEffectsPrefabs[Index];
        AM.i.PlaySFX(SoundEffects.Where(SE => SE.name == AudioEffectName).FirstOrDefault());
        if (EffectGuide != null) {
            var EFX = (GameObject)GameObject.Instantiate(EffectGuide, Pos, Rot);
            if (Parent != null)
                EFX.transform.parent = Parent;
        }
    }


    public void PlaySlashEffect(string Name, Vector3 Pos,Transform Parent=null, Quaternion Rot = new Quaternion(), string AudioEffectName = "") {
        var EffectGuide = SlashEffectsPrefabs.Where(E => E.name == Name).FirstOrDefault();
         AM.i.PlaySFX(SoundEffects.Where(SE => SE.name == AudioEffectName).FirstOrDefault());
         if (EffectGuide != null) {
             var EFX = (GameObject)GameObject.Instantiate(EffectGuide, Pos, Rot);
             if (Parent != null)
                 EFX.transform.parent = Parent;
         }
    }


    void Awake(){
        MSC = GameObject.Find("MainSceneCode").GetComponent<MainSceneCode>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
