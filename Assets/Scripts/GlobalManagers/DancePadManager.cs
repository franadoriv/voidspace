﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class DancePadManager : MonoBehaviour {

    public static DancePadManager i;

    public List<DancePadKey> Keys = new List<DancePadKey>();

    public bool Detected = false;

    public class DancePadKey {
        public KeyCode PadKeyCode;
        public string KeyName;
        public int AnswerMapNum = 0;
        public DancePadKey(KeyCode PadKeyCode, string KeyName,int AnswerMapNum=0) {
            this.PadKeyCode = PadKeyCode;
            this.KeyName = KeyName;
            this.AnswerMapNum = AnswerMapNum;
        }
        public bool KeyPress() {
            return Input.GetKey(this.PadKeyCode);
        }
        public bool KeyDown() {
            return Input.GetKeyDown(this.PadKeyCode);
        }
        public bool KeyUp() {
            return Input.GetKeyUp(this.PadKeyCode);
        }
    }

    public bool GetDanceKey(string KeyName) {
        return Input.GetKey(Keys.Find(K => K.KeyName == KeyName).PadKeyCode);
    }

    public bool GetDanceKeyDown(string KeyName) {
        return Input.GetKeyDown(Keys.Find(K => K.KeyName == KeyName).PadKeyCode);
    }

    public bool GetDanceKeyUp(string KeyName) {
        return Input.GetKeyUp(Keys.Find(K => K.KeyName == KeyName).PadKeyCode);
    }

    public DancePadKey GetKeysDown(string[] SearchKeys,bool CheckPressed=false) {
        foreach (var K in Keys) {
            if (K.KeyDown() && SearchKeys.Any(SK => CheckPressed?(SK == K.KeyName && K.KeyDown()):(SK == K.KeyName))) {
                K.AnswerMapNum = Array.IndexOf(SearchKeys, K.KeyName);
                return K;
            }
        }
        return null;
    }

    public bool AnyDanceKeyPressed() {
        return Keys.Any(K => Input.GetKey(K.PadKeyCode));
    }

    public DancePadKey GetAnyDanceKey() {
        return Keys.Find(K => Input.GetKey(K.PadKeyCode));
    }

    void Awake() {
        DancePadManager.i = this;
    }
	// Use this for initialization
	void Start () {
        Keys.Add(new DancePadKey(KeyCode.JoystickButton0, "W"));
        Keys.Add(new DancePadKey(KeyCode.JoystickButton1, "S"));
        Keys.Add(new DancePadKey(KeyCode.JoystickButton2, "N"));
        Keys.Add(new DancePadKey(KeyCode.JoystickButton3, "E"));
        Keys.Add(new DancePadKey(KeyCode.JoystickButton4, "SE"));
        Keys.Add(new DancePadKey(KeyCode.JoystickButton5, "SW"));
        Keys.Add(new DancePadKey(KeyCode.JoystickButton6, "NW"));
        Keys.Add(new DancePadKey(KeyCode.JoystickButton7, "NE"));
        Keys.Add(new DancePadKey(KeyCode.JoystickButton8, "SELECT"));
        Keys.Add(new DancePadKey(KeyCode.JoystickButton9, "START"));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
