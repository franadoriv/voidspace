﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using GCM = GlobalCanvasManager;
using AM = AudioManager;

public class SceneManager : MonoBehaviour {

    public static SceneManager i;

    public Texture TEX_FadeBlack;
    public Texture TEX_FadeWhite;
    public UITexture TEX_Fade;

    public List<Texture> LoadingImages = new List<Texture>();

    float ILBW = 0;
    GlobalManager GM;

    public bool LevelAsyncContinue = false;


    public void FadeOut(Action OnFinish = null, bool WhiteFade = true, bool AudioFade = true, float FadeTime = 2F) {
        GCM.i.Fade(false, OnFinish, WhiteFade);

    }

    public void FadeOutSimple() {
        GCM.i.Fade(false, WhiteFade: false);
    }

    public void FadeInSimple() {
        GCM.i.Fade(true, WhiteFade: true);
    }

    public void FadeIn(Action OnFinish = null, bool WhiteFade = true,bool AudioFade = true, float FadeTime = 1F) {
        GCM.i.Fade(true, OnFinish, WhiteFade);
    }


    IEnumerator ActionDelayed(float seconds,Action Action){
        yield return new WaitForSeconds(seconds);
        Action();
    }

    public void DelayAction(float seconds, Action Action) {
        StartCoroutine(ActionDelayed(seconds, Action));
    }

    public IEnumerator BeginLevelLoad(string LevelName) {
        LevelAsyncContinue = false;
        Debug.Log("Begining of the loading of level '" + LevelName + "'");
        
        AsyncOperation async = Application.LoadLevelAsync(LevelName);
        yield return async;
        while (!LevelAsyncContinue)
            yield return new WaitForEndOfFrame();
        Debug.Log("Loading of the level '" + LevelName + "' complete!");
    }

    void SetVolume(float V) { 
    if(Camera.main!=null)
                if(Camera.main.GetComponent<AudioSource>()!=null)
                    Camera.main.GetComponent<AudioSource>().volume = (float)V *0.9f;
    }

    public void ChangeScene(string SceneName,bool DoFadeIn = true,bool WhiteFade = false,bool FadeOutAudio=true,float FadeTime = 1.0F, bool ForceInitialValue = true) {
        Debug.Log("Changing scene to: " + SceneName);
        GCM.i.Fade(false, () => {
            UnityEngine.SceneManagement.SceneManager.LoadScene(SceneName);
        },false,ForceInitialValue);
        if (FadeOutAudio)
            AM.i.FadeOut(1,()=> { AM.i.BGM.Stop(); });
    }

    public static void CheckSceneManager() {
        if (GameObject.Find("SceneManager") == null) {
            GameObject SM = Instantiate(Resources.Load("SceneManager")) as GameObject;
            SM.name = "SceneManager";
        }
    }

    public void PlayFlash(Action OnMiddle, Action OnFinish = null) {
        PlayFlash(0.25F, true, OnMiddle, OnFinish);
    }
    public void PlayFlash(float Duration = 0.25f, bool WhiteFade = true, Action OnMiddle = null, Action OnFinish = null) {
        TEX_Fade.mainTexture = WhiteFade ? TEX_FadeWhite : TEX_FadeBlack;
        FadeOut(() => {
            DelayAction(0.25f, () => {
                if (OnMiddle != null) OnMiddle();
                FadeIn(OnFinish,WhiteFade,false, Duration / 2);
            });
        }, WhiteFade, false, Duration / 2);
    
    
    }

    void Awake() {
        i = this;
        DontDestroyOnLoad(gameObject);
        GM = gameObject.AddComponent<GlobalManager>();
    }


    void Start() {
    }

    void Update() {
    }

    
}
