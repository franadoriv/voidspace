﻿using UnityEngine;
using System.Collections;
using System;
using GM = GlobalManager;

public class GameCameraManager : MonoBehaviour {

    public static GameCameraManager i;
    // Use this for initialization

    public GameObject TPCamera;
    public GameObject FPCamera;
    public MainSceneCode MSC;

    int CurrentTransID;
    Vector3 LastTPPosition = new Vector3();

    /*
    public void GoToFirstPerson(Action OnFinish=null) {
        TPCamera.GetComponent<SmoothFollow>().enabled = false;
        TPCamera.transform.parent = FPCamera.transform.parent;//((MSC_Game)MSC).PM.PlayerObj.transform;
        TPCamera.GetComponent<MotionBlur>().enabled = true;
        TPCamera.GetComponent<FastBloom>().enabled = false;
        LastTPPosition = TPCamera.transform.localPosition;
        iTween.MoveTo(TPCamera, FPCamera.transform.localPosition, 1F,true, () => {
            TPCamera.SetActive(false);
            ((MSC_Game)MSC).PM.PlayerCharacterTP.SetActive(false);
            GM.i.DelayFrameAction(1, () => {
                FPCamera.SetActive(true);
                if (OnFinish != null)
                    OnFinish();
            });
        });
        //iTween.RotateTo(TPCamera, FPCamera.transform.localRotation.eulerAngles,2F);
    }*/

/*
    public void GoToThirdPerson(Action OnFinish = null) {
        TPCamera.GetComponent<SmoothFollow>().enabled = false;
        TPCamera.transform.parent = FPCamera.transform.parent;
        TPCamera.GetComponent<MotionBlur>().enabled = true;
        TPCamera.GetComponent<FastBloom>().enabled = false;
        FPCamera.SetActive(false);
        GM.i.DelayFrameAction(1, () => { 
            ((MSC_Game)MSC).PM.PlayerCharacterTP.SetActive(true);
            ((MSC_Game)MSC).PM.PlayerCharacterFP.SetActive(false);
            iTween.MoveTo(TPCamera, LastTPPosition, 1F, true, () => {
                TPCamera.transform.parent = null;
                TPCamera.GetComponent<SmoothFollow>().enabled = true;
                TPCamera.GetComponent<MotionBlur>().enabled = false;
                TPCamera.GetComponent<FastBloom>().enabled = true;
                if (OnFinish != null)
                    OnFinish();
            });
            TPCamera.SetActive(true);
        });
    }
    */
    public void LookToLeft(Action OnFinish=null) {
        iTween.Stop(FPCamera);
        GM.i.DelayFrameAction(1, () => {
            iTween.RotateTo(FPCamera, new Vector3(-6.240051F, -84.4F, -7.92F), 0.5f, true, OnFinish);
        });
    }

    public void LookToFront(Action OnFinish = null,float Time = 0.5F) {
        iTween.Stop(FPCamera);
        GM.i.DelayFrameAction(1, () => {
            iTween.RotateTo(FPCamera, new Vector3(-6.240051F, -10.33011F, 15.47F), Time, true, OnFinish);
        });
    }
    
    public void LookToRight(Action OnFinish = null) {
        iTween.Stop(FPCamera);
        GM.i.DelayFrameAction(1, () => {
            iTween.RotateTo(FPCamera, new Vector3(-13.56F, 75.7F, 0.0F), 0.5f, true, OnFinish);
        });
    }
    /*
    public void LookTo(MonsterManager.Side Side,Action OnFinish = null) {
        int Direction = (int)UnityEngine.Random.Range(1, 3);
        switch (Side) {
            case MonsterManager.Side.L:
                LookToLeft(OnFinish);
                break;
            case MonsterManager.Side.F:
                LookToFront(OnFinish);
                break;
            case MonsterManager.Side.R:
                LookToRight(OnFinish);
                break;
        }
            
    }
    */
    public void LookTo(GameObject Target, Action OnFinish = null) {
        iTween.Stop(FPCamera);
        GM.i.DelayFrameAction(1, () => {
            GameObject NWO = new GameObject();
            NWO.transform.parent = FPCamera.transform.parent;
            NWO.transform.localPosition = FPCamera.transform.localPosition;
            NWO.transform.localRotation = FPCamera.transform.localRotation;
            NWO.transform.LookAt(Target.transform);
            var TR = NWO.transform.localRotation.eulerAngles;
            GameObject.Destroy(NWO);
            var D = Vector3.Distance(FPCamera.transform.localRotation.eulerAngles, TR);
            Debug.Log("D: " + D);
            if (D > 2)
                iTween.RotateTo(FPCamera, TR, 0.25F, true, OnFinish);
            else
                OnFinish();
        });
    
    }

    public void LookTo(string TargetName,Action OnFinish = null) {
        var T = GameObject.Find(TargetName);
        LookTo(T,OnFinish);
    }

    void Awake() {
        i = this;
    }
    

	void Start () {
        MSC = GameObject.Find("MainSceneCode").GetComponent<MainSceneCode>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
