﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using System;

public class AudioManager : MonoBehaviour {

    public static AudioManager i;

    //public bool Debug = false;
    public GlobalManager GM;

    //Audio Objects
    public AudioSource BGM;
    public AudioSource SFX;


    public void PlaySFX(AudioClip AC) {
        if (AC != null && SFX != null)
            SFX.PlayOneShot(AC,1);
            //AudioSource.PlayClipAtPoint(AC, Camera.main.transform.position,1);
    }

    public void PlaySFX(AudioClip AC, AudioSource AS) {
        if (AC != null && SFX != null)
            AS.PlayOneShot(AC);
    }

    public void PlayBGM(AudioClip AC, bool FadeIn = true, bool FadeOut = true, float FadeTimeSeconds = 1.0f) {
        Debug.Log("Changing BGM to " + AC);
        if (AC == null) return;
        //iTween.StopByName("ITW_MusicFade");
        if (FadeIn) {
            iTween.ValueTo(gameObject, BGM.volume, 0, FadeTimeSeconds, (V) => {
                BGM.volume = (float)V;
            }, () => {
                GM.DelayAction(0.25f, () => {
                    BGM.Stop();
                    BGM.GetComponent<AudioSource>().clip = AC;
                    BGM.Play();
                    iTween.ValueTo(gameObject, 0, 0.5f, FadeTimeSeconds, (V) => {
                        BGM.volume = ((float)V);
                        SFX.volume = ((float)V);
                    }, null, "ITW_MusicFade");
                });
            }, "ITW_MusicFade");
        }
        else {
            BGM.Stop();
            BGM.GetComponent<AudioSource>().clip = AC;
            BGM.volume = 0.9f;
            BGM.Play();
        }
    }

    public void SetVolume(float V) {
        BGM.volume = V * 0.9f;
        SFX.volume = V;
    }

    public void FadeIn(float time = 1) {
        iTween.StopByName("FadeVolume");
        SetVolume(0);
        BGM.Play();
        iTween.ValueTo(gameObject, 0, 0.5F, time, (V) => {
            BGM.volume = (float)V;
            SFX.volume = (float)V;
        }, null, "FadeVolume");
    }

    public void FadeOut(float time = 1,Action OnFinish=null) {
        iTween.StopByName("FadeVolume");
        Debug.Log("BGM.volume: " + BGM.volume);
        iTween.ValueTo(gameObject, BGM.volume, 0.0F, time, (V) => {
            BGM.volume = (float)V;
            SFX.volume = (float)V;
        }, OnFinish, "FadeVolume");
    }

    void Awake() {
        i = this;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
