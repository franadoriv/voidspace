﻿using UnityEngine;
using System.Collections;

public class CursorManager : MonoBehaviour {

    public static CursorManager i;
    public Sprite TEX_CurrentCursor;

    public Sprite TEX_Default;
    public Sprite TEX_HandCursor;
    public Sprite TEX_BlockedCursor;
    public Sprite TEX_Bullseye;
    public Sprite TEX_Targeted;
    public CursorType CurrentCursor = CursorType.Default;

    public enum CursorType { Default, Hand, Blocked,Bullseye,Targeted }



    public static Texture2D textureFromSprite(Sprite sprite,int width = 0, int height = 0) {
        if (width == 0) width = (int)sprite.rect.width;
        if (height == 0) height = (int)sprite.rect.height;
        if (sprite.rect.width != sprite.texture.width) {
            Texture2D newText = new Texture2D(width, height);
            Color[] newColors = sprite.texture.GetPixels((int)sprite.textureRect.x,
                                                         (int)sprite.textureRect.y,
                                                         width,
                                                         height);
            newText.SetPixels(newColors);
            newText.Apply();
            return newText;
        }
        else
            return sprite.texture;
    }

    public void SetCursor(CursorType CT,bool Center = false,bool Rotate = false) {
        //Debug.Log("Setting cursor to: " + CT.ToString());
        Sprite TEX_Cursor = null;
        CurrentCursor = CT;
        switch (CT) {
            case CursorType.Hand:
                TEX_Cursor = TEX_HandCursor;
                break;
            case CursorType.Blocked:
                TEX_Cursor = TEX_BlockedCursor;
                break;
            case CursorType.Bullseye:
                TEX_Cursor = TEX_Bullseye;
                break;
            case CursorType.Targeted:
                TEX_Cursor = TEX_Targeted;
                break;
            case CursorType.Default:
            default:
                TEX_Cursor = TEX_Default;
                break;
        }
        var T = textureFromSprite(TEX_Cursor);
        //T.Resize(10, 10);
        Cursor.SetCursor(T, new Vector2(Center? T.width / 2 :0 , Center ? T.height / 2 : 0), CursorMode.ForceSoftware);
    }




    void Awake() {
        i = this;
    }

    // Use this for initialization
    void Start() {
        SetCursor(CursorType.Hand);
        //Cursor.SetCursor(TEX_HandCursor.texture, new Vector2(), CursorMode.ForceSoftware);
    }


    // Update is called once per frame
    void Update() {

    }
}
