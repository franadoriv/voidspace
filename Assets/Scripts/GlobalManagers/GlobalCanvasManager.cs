﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using System;
using AM = AudioManager;

public class GlobalCanvasManager : MonoBehaviour {
    public static GlobalCanvasManager i;
    public GameObject PNL_Alert;

    public RawImage TEX_ScreenFader;

    public void Fade(bool In=false,Action OnFinish = null, bool WhiteFade = false,bool ForceInitialValue = true) {
        iTween.StopByName(TEX_ScreenFader.gameObject,"TEX_ScreenFader");
        GlobalManager.i.DelayFrameAction(3, () => {
            float InitialValue = TEX_ScreenFader.color.a;
            if (ForceInitialValue)
                InitialValue = In ? 1.0F : 0.0F;
            if (TEX_ScreenFader.gameObject) 
            iTween.ValueTo(TEX_ScreenFader.gameObject, InitialValue, In ? 0.0F : 1.0F, 1.0F, (V) => {
                var C = TEX_ScreenFader.color;
                C = WhiteFade?Color.white:Color.black;
                C.a = (float)V;
                TEX_ScreenFader.color = C;
            }, OnFinish, "TEX_ScreenFader");
        });
    }

    void Awake() {
        i = this;
        TEX_ScreenFader.color = new Color(0, 0, 0, 1);
        DontDestroyOnLoad(transform.gameObject);
        PNL_Alert.SetActive(true);
    }

	// Use this for initialization
	void Start () {
        if (!TEX_ScreenFader)
            GetComponentsInChildren<Transform>(true).Where(T => T.name == "TEX_ScreenFader").FirstOrDefault().GetComponent<RawImage>();
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
