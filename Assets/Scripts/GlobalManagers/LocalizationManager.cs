﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizationManager : MonoBehaviour {

    public static LocalizationManager i;

    public Dictionary<string, Action<string>> Requests = new Dictionary<string, Action<string>>();

    public void OnData(string data) {
        Debug.Log("OnData for LocationManager: " + data);
        var Request = SimpleJSON.JSON.Parse(data);
        if (Requests.ContainsKey(Request["RequestID"].Value)) {
            Requests[Request["RequestID"].Value](Request["Result"].Value);
            Requests.Remove(Request["RequestID"].Value);
        }
        else
            Debug.Log("LocationManager request ID " + Request["RequestID"].Value + " not found");
    }

    public void FillLabel(string  Name,Text LBL,string Default = "") {
        Debug.Log("Localization - GetText -> " + Name + " in " + Application.platform.ToString());
        if (Application.isWebPlayer  || Application.platform.ToString() == "WebGLPlayer") {
            Debug.Log("Web player detected");
            string RequestID = Guid.NewGuid().ToString();
            string script = "var Translation = window.parent.getTranslation('" + Name + "');";
            script += "var ExternalEval = {RequestID:'" + RequestID + "',Result:Translation};";
            script += "SendMessage('LocalizationManager', 'OnData',JSON.stringify(ExternalEval));";
            Debug.Log("External JS execution: " + script);
            Action<string> OnData = (STR) => { LBL.text = STR; };
            Requests.Add(RequestID, OnData);
            Application.ExternalEval(script);
        }
       else {
            Debug.Log("Web player not detected");
            LBL.text = Default;
        }
        
    }

    public void GetText(string Name, string Default = "",Action<string> OnFinish=null) {
        Debug.Log("Localization - GetText -> " + Name + " in " + Application.platform.ToString());
        if (Application.isWebPlayer || Application.platform.ToString() == "WebGLPlayer") {
            Debug.Log("Web player detected");
            string RequestID = Guid.NewGuid().ToString();
            string script = "var Translation = window.parent.getTranslation('" + Name + "');";
            script += "var ExternalEval = {RequestID:'" + RequestID + "',Result:Translation};";
            script += "SendMessage('LocalizationManager','OnData',JSON.stringify(ExternalEval));";
            Debug.Log("External JS execution: " + script);
            Requests.Add(RequestID, OnFinish);
            Application.ExternalEval(script);
        }
        else {
            Debug.Log("Web player not detected");
            OnFinish(Default);
        }
            
    }

    void Awake() {
        i = this;
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
