﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Collections.Generic;
public class GlobalManager : MonoBehaviour {

    public static GlobalManager i;

    public Transform SearchTransform(Transform root, string Name, bool CaseSensitve = true) {
        Component[] CMPS = root.GetComponentsInChildren(typeof(Transform), true);
        Component x = CMPS.Where(C => (CaseSensitve ? C.name == Name : C.name.Contains(Name))).FirstOrDefault();
        return x != null ? x.transform : null;
    }

    public Transform[] SearchTransforms(Transform root, string Name, bool CaseSensitve = true,bool IncludeInactive = true) {
        Debug.Log("Searching " + Name + " in " + root.name);
        var R = new List<Transform>();
        Component[] CMPS = root.GetComponentsInChildren(typeof(Transform), IncludeInactive);
        CMPS = CMPS.Where(C => (CaseSensitve ? C.name == Name : C.name.Contains(Name))).ToArray();
        foreach (Component C in CMPS) { 
            R.Add(C.transform); 
        }
        return R.ToArray();
    }

    public static IEnumerator ActionDelayed(float seconds, Action Action) {
        yield return new WaitForSecondsRealtime(seconds);
        Action();
    }
    public static IEnumerator ActionFrameDelayed(int frames, Action Action) {
        int N = 0;
        while (N < frames) {
            yield return new WaitForEndOfFrame(); N++;
        }
        Action();
    }

    public Coroutine DelayAction(float seconds, Action Action) {
        return StartCoroutine(ActionDelayed(seconds, Action));
    }
    public Coroutine DelayFrameAction(int frames, Action Action) {
        return StartCoroutine(ActionFrameDelayed(frames, Action));
    }

    public static GameObject GetTagObj(string TagName,string ObjName){
        return GameObject.FindGameObjectsWithTag(TagName).Where(O=>O.name == ObjName).FirstOrDefault();
    }

    public static  Vector3 GetRelLocPos(GameObject Source, GameObject Target){
        Debug.Log("GetRelLocPos: Source " + Source + " Target " + Target);
        var SP = Source.transform.parent;
        var TP = Target.transform.parent;
        Target.transform.parent = SP;
        Vector3 T = Target.transform.localPosition;
        Target.transform.parent = TP;
        return T;
    }

 
    public bool IsNull(GameObject GO) {
        return !(GO != null && !GO.Equals(null));
    }

    void Awake() {
        i = this;
    }

	// Use this for initialization
	void Start () {
        Debug.Log(transform.parent.gameObject.name);
        Application.runInBackground = true;
        DontDestroyOnLoad(transform.parent.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
