﻿using UnityEngine;
using System.Collections;

public static class GlobalVariables {
    public enum QuestionsOrigins {
        INTERNAL_FILE, EXTERNAL_FILE, INTERNAL_VARIABLE, EXTERNAL_REQUEST, LOCAL_MAPI
    }
    public static class ConfigValues {
        public static string FilesFolders = "testResources/"; //Memarden GameFramework
        public static string QuestionsFolder = "testData/"; //Memarden GameFramework
        public static string StreamedLevelsFolders = @"http://192.168.1.108/Files/"; //Memarden GameFramework
        public static string MemardenExternalApiUrl = "http://test.memarden.com/api/?token=TOKEN_NUMBER&service=2";
        public static string QueryBridge = "http://marvellousgames.net/BridgeRequest/Bridge.asmx/";
        public static string TestDataFile = "Q2"; //Used to test Memarden Framework remotely
        public static float TiltLeft = 110;
        public static float TiltRight = 80;
        public static string QuestionaryToken = "";
        public static QuestionsOrigins QuestionsOrigin = QuestionsOrigins.INTERNAL_FILE;
    }

    public static class OptionsValues {
        public static float BGM_Volume = 1.0f;
        public static float SFX_Volume = 1.0f;
        public static int DifficultyLevel = 1;
    }

    public static class PlayerScore {
        public static int Coins = 0;
        public static float TimeBonus = 0.0f;

    }

}