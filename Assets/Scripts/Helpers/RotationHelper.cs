﻿using UnityEngine;
using System.Collections;

public class RotationHelper : MonoBehaviour {


    public Vector3 LocalRotationVelocity = new Vector3();

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(LocalRotationVelocity,Space.Self);
	}
}
