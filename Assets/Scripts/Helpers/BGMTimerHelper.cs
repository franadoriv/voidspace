﻿using UnityEngine;
using System.Collections;
using System;

public class BGMTimerHelper : MonoBehaviour {

    public UILabel BGM_TimerLabel;
    public AudioSource BGM_AudioSource;
    public Animation ANIM_MainScene;
    public bool SyncAdio = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (BGM_TimerLabel != null && BGM_AudioSource != null)
            BGM_TimerLabel.text = "BGM TIME -> " + TimeSpan.FromSeconds(BGM_AudioSource.time).ToString();
        if (ANIM_MainScene != null && SyncAdio) {
            ANIM_MainScene["HangarIntroAnim"].time = BGM_AudioSource.time;
            ANIM_MainScene["HangarIntroAnim"].speed = 0.0F;
            //ANIM_MainScene.Play();
        }
	}
}
