﻿using UnityEngine;
using System.Collections;

public class ThrusterHelper : MonoBehaviour {

    public ParticleSystem PS;
    public Light Light;
    public float ThrusterPower = 0;

	// Use this for initialization
	void Start () {
        PS = GetComponent<ParticleSystem>();
        Light = GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
        if (ThrusterPower > 100)
            ThrusterPower = 100;
        else if (ThrusterPower < 0)
            ThrusterPower = 0;
        if (PS != null) {
            PS.startLifetime = ThrusterPower * 0.5F / 100F;
        }
        if (Light != null)
            Light.intensity = ThrusterPower * 0.75F / 100F;
    }
}
