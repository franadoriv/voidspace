﻿using UnityEngine;
using System.Collections;
using System.Linq;
using GM = GlobalManager;

public class SingleEventListener : MonoBehaviour {

    MainSceneCode MSC;

    public void OnExecute(string TargetAndFunction) {
        string Target = TargetAndFunction.Split(';')[0];
        string Function = TargetAndFunction.Split(';')[1];
        GameObject GO = GameObject.Find(Target);
        if(GO!=null)
            GO.SendMessage(Function);
    }



    public void ActivateAnimatorTrigger(string TargetAndTrigger) {
        string Target = TargetAndTrigger.Split(';')[0];
        string Trigger = TargetAndTrigger.Split(';')[1];
        GameObject.Find(Target).GetComponent<Animator>().SetTrigger(Trigger);
    }

    public void ToggleGameObject(string RootAndGameObjectNameAndActived) {
        GM.i.SearchTransform(GameObject.Find(RootAndGameObjectNameAndActived.Split(';')[0]).transform, RootAndGameObjectNameAndActived.Split(';')[1],false).gameObject.SetActive(RootAndGameObjectNameAndActived.Split(';')[2].Contains("true"));
    }

    public void ToggleGameObjects(string RootAndGameObjectsNameAndActived) {
        foreach(Transform T in GM.i.SearchTransforms(GameObject.Find(RootAndGameObjectsNameAndActived.Split(';')[0]).transform, RootAndGameObjectsNameAndActived.Split(';')[1],false)){
            T.gameObject.SetActive(RootAndGameObjectsNameAndActived.Split(';')[2].Contains("true"));
        }
    }

    //Root or Parent + GameObject Name + Component Name + Active (true or false)
    public void ToggleGameObjectComponent(string Chain) {
        Component C = GM.i.SearchTransform(GameObject.Find(Chain.Split(';')[0]).transform, Chain.Split(';')[1], false).gameObject.GetComponent(Chain.Split(';')[2]);
        (C as MonoBehaviour).enabled = Chain.Split(';')[3].Contains("true");
    }

    public void ToggleGameObjectsComponent(string Chain) {
        foreach (Transform T in GM.i.SearchTransforms(GameObject.Find(Chain.Split(';')[0]).transform, Chain.Split(';')[1], false)) {
            Component C = T.gameObject.GetComponent(Chain.Split(';')[2]);
            (C as MonoBehaviour).enabled = Chain.Split(';')[3].Contains("true");
        }
    }

    public void PlaySFX(string SFXName) {
        Debug.Log("Searching for play " + SFXName);
        AudioManager AM = GameObject.Find("AudioManager").GetComponent<AudioManager>();
        AudioClip AC = GameObject.Find("SoundLibraryHelper").GetComponent<SoundLibraryHelper>().Sounds.Where(W => W.name == SFXName).FirstOrDefault();
        Debug.Log("AC: " + AC);
        AM.PlaySFX(AC);
    }

    public void PlaySFXOnAudioSource(string AudioSourceAndSFX) {
        Debug.Log("Searching for play " + AudioSourceAndSFX.Split(';')[1] + " in " + AudioSourceAndSFX.Split(';')[0]);
        AudioSource AD = GameObject.Find(AudioSourceAndSFX.Split(';')[0]).GetComponent<AudioSource>();
        AudioClip AC = GameObject.Find("SoundLibraryHelper").GetComponent<SoundLibraryHelper>().Sounds.Where(W => W.name == AudioSourceAndSFX.Split(';')[1]).FirstOrDefault();
        Debug.Log("AC: " + AC);
        AD.PlayOneShot(AC);
    }

    public void PlaySFXOnAudioLibrarySource(string AudioSourceAndSFX) {
        Debug.Log("Searching for play " + AudioSourceAndSFX.Split(';')[1] + " in " + AudioSourceAndSFX.Split(';')[0]);
        //AudioSource AD = GameObject.Find(AudioSourceAndSFX.Split(';')[0]).GetComponent<AudioSource>();
        AudioSource AS = GameObject.Find("SoundLibraryHelper").GetComponent<SoundLibraryHelper>().SoundsSources.Where(W => W.name == AudioSourceAndSFX.Split(';')[0]).FirstOrDefault();
        AudioClip AC = GameObject.Find("SoundLibraryHelper").GetComponent<SoundLibraryHelper>().Sounds.Where(W => W.name == AudioSourceAndSFX.Split(';')[1]).FirstOrDefault();
        Debug.Log("AC: " + AC);
        AS.PlayOneShot(AC);
    }

    public void PlayBGM(string BGMName) {
        Debug.Log("Searching for play " + BGMName);
        AudioManager AM = GameObject.Find("AudioManager").GetComponent<AudioManager>();
        AudioClip AC = GameObject.Find("SoundLibraryHelper").GetComponent<SoundLibraryHelper>().Musics.Where(W => W.name == BGMName).FirstOrDefault();
        Debug.Log("AC: " + AC);
        AM.PlayBGM(AC,false);
    }


	// Use this for initialization
	void Start () {
        MSC = GameObject.Find("MainSceneCode").GetComponent<MainSceneCode>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
