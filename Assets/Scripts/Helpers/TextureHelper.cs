﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextureHelper : MonoBehaviour {


    public Vector2 MoveTexture = new Vector2(0,0);
    public Renderer Renderer;
    public Image IMG;
    public Material MAT;
    Vector2 TR = new Vector2();
    Vector2 TI = new Vector2();
	// Use this for initialization
	void Start () {
        if (!Renderer)
            Renderer = gameObject.GetComponent<Renderer>();
        if (!Renderer)
            Renderer = gameObject.GetComponent<LineRenderer>();
        if (Renderer)
            TR = Renderer.material.GetTextureOffset("_MainTex");

        IMG = GetComponent<Image>();
        if(IMG)
            TI = IMG.material.GetTextureOffset("_MainTex");
 
	}

	
	// Update is called once per frame
	void Update () {
        if (Renderer) {
            TR.x += MoveTexture.x*Time.deltaTime;
            TR.y += MoveTexture.y*Time.deltaTime;
            Renderer.material.SetTextureOffset("_MainTex", TR);
        }
        if (IMG) {
            TI.x += MoveTexture.x * Time.deltaTime;
            TI.y += MoveTexture.y * Time.deltaTime;
            IMG.material.SetTextureOffset("_MainTex", TI);
        }
	}
}
