﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using GM = GlobalManager;
using MGL = MGL_MainGame;
using PM = PositionMatrix;
using PT = PositionMatrix.PositionTypes;
using MIF = MIF_MainGame;
using AM = AudioManager;


public class PlayerHelper : MonoBehaviour {

    public static PlayerHelper i;

    //Variables
    public float ThrustersPower = 0;
    public int CurrentEnergy = 100;
    public ViewTypes CurrentView;

    //Global Managers
    public MainSceneCode MSC;

    //Local controllers
    public ThrusterHelper[] Thrusters = new ThrusterHelper[4]; 

    //AudioSources
    public AudioSource AUD_PlayerShip;

    //SoundEffects
    public AudioClip SFX_ActivateWindows;
    public AudioClip SFX_DeactivateWindows;
    public AudioClip SFX_ActivateScreens;
    public AudioClip SFX_DeactivateScreens;
    public AudioClip SFX_IceOnWindows;
    public AudioClip SFX_MeteorCrash;

    //Prefabs
    public GameObject FX_HitDamage;
    public GameObject FX_ShipDestroyed;

    //Objects
    public GameObject PlayerShip;
    public GameObject PlayerShipBody;
    public WSP_HighRezLaser Laser;
    public GameObject Cockpit;
    public Camera CAM_3P;
    public Camera CAM_SPC;
    //public Camera CAM_COCKPIT;
    //Enums
    public enum ViewTypes { outside, cocktip, cocktipfront}

    //Anchors
    public GameObject ANCH_Cockpit;
    public GameObject ANCH_CockpitFront;

    //Functions
    public void ToggleWindowsOn() {ToggleWindows(true);}
    public void ToggleWindows(bool Active = true) {
        iTween.StopByName("TWN_ShipWindows");
        Material MAT_ShipWindows = GM.i.SearchTransform(transform, "spaceship_cockpit:outside_windows").GetComponent<MeshRenderer>().materials.Where(M => M.name.Contains("cockpit_windows1")).FirstOrDefault();
        GM.i.DelayFrameAction(2, () => {
            AM.i.PlaySFX(Active?SFX_ActivateWindows:SFX_DeactivateWindows);
            iTween.ValueTo(gameObject, MAT_ShipWindows.GetTextureOffset("_MainTex").y, Active ? 0 : 0.8F, 0.5f, (V) => {
                MAT_ShipWindows.SetTextureOffset("_MainTex", new Vector2(0, (float)V));
            });
        });
    }
    public void ToggleScreensOn() { ToggleScreens(true); }
    public void ToggleScreens(bool Active = true) {
        AM.i.PlaySFX(Active ? SFX_ActivateScreens : SFX_DeactivateScreens);
        foreach(Transform T in GM.i.SearchTransforms(transform,"DScreen_",false)){
            if (Active) 
                T.GetComponent<UITweener>().PlayForward();
            else
                T.GetComponent<UITweener>().PlayReverse();
        }
    }
    public void IceWindows(float time = 4) {
        iTween.StopByName("TWN_ShipIceWindows");
        Material MAT_ShipWindowsIce = GM.i.SearchTransform(transform, "spaceship_cockpit:outside_windows").GetComponent<MeshRenderer>().materials.Where(M => M.name.Contains("cockpit_windows_ice")).FirstOrDefault();
        GM.i.DelayFrameAction(2, () => {
            iTween.ValueTo(gameObject, MAT_ShipWindowsIce.color.a, 1, time / 2, (V) => {
                MAT_ShipWindowsIce.color = new Color(1, 1, 1, (float)V); 
            }, () => {
                GM.i.DelayFrameAction(2, () => {
                    iTween.ValueTo(gameObject, 1, 0, time / 2, (V) => {
                        MAT_ShipWindowsIce.color = new Color(1, 1, 1, (float)V); 
                    }, null, "TWN_ShipIceWindows");
                });

            }, "TWN_ShipIceWindows");
        });
    }

    public void ChangeView(ViewTypes viewtype,Action OnFinish = null,float Vel = 1.5F) {
        //OUTSIDE FOV 78
        //INSIDE FOV 40
        Debug.Log("Changing view to -> " + viewtype.ToString());
        if (CurrentView == viewtype) { 
            if(OnFinish != null)OnFinish();
            return;
        }
        iTween.StopByName(CAM_3P.gameObject, "MOVE");
        iTween.StopByName(CAM_3P.gameObject, "ROT");
        iTween.StopByName(CAM_3P.gameObject, "FOV");
        GM.i.DelayFrameAction(2, () => {
            CurrentView = viewtype;
            switch (viewtype) {
                case ViewTypes.cocktip:
                    var TEMP_PARENT = ANCH_Cockpit.transform.parent;
                    ANCH_Cockpit.transform.parent = CAM_3P.transform.parent;
                    Action OF = () => {
                        ANCH_Cockpit.transform.parent = TEMP_PARENT;
                        if (OnFinish != null) OnFinish();
                    };
                    iTween.MoveTo(CAM_3P.gameObject, GM.GetRelLocPos(CAM_3P.gameObject, ANCH_Cockpit), Vel, true, OF, "MOVE");
                    iTween.RotateTo(CAM_3P.gameObject, ANCH_Cockpit.transform.rotation.eulerAngles, Vel, false, null,"ROT");
                    iTween.ValueTo(CAM_3P.gameObject, CAM_3P.fieldOfView, 67.0F, Vel, (V) => { CAM_3P.fieldOfView = (float)V; }, null, "FOV");
                    GM.i.DelayAction(0.2F, () => {
                        PlayerShipBody.SetActive(false);
                        Cockpit.SetActive(true);
                    });
                    break;
                case ViewTypes.cocktipfront:
                    var TEMP_PARENT2 = ANCH_CockpitFront.transform.parent;
                    ANCH_CockpitFront.transform.parent = CAM_3P.transform.parent;
                    Action OF2 = () => {
                        ANCH_CockpitFront.transform.parent = TEMP_PARENT2;
                        if (OnFinish != null) OnFinish();
                    };
                    iTween.MoveTo(CAM_3P.gameObject, GM.GetRelLocPos(CAM_3P.gameObject, ANCH_CockpitFront), Vel, true, OF2, "MOVE");
                    iTween.RotateTo(CAM_3P.gameObject, ANCH_CockpitFront.transform.rotation.eulerAngles, Vel, false, null, "ROT");
                    iTween.ValueTo(CAM_3P.gameObject, CAM_3P.fieldOfView, 67.0F, Vel, (V)=>{CAM_3P.fieldOfView = (float)V;},null,"FOV");
                    GM.i.DelayAction(0.2F,()=>{
                        PlayerShipBody.SetActive(false);
                        Cockpit.SetActive(true);
                    });
                    break;
                case ViewTypes.outside:
                    var P = PM.i.GetPOP(PM.LayerTypes.Other, PT.CAM_3P);
                    var TEMP_PARENT3 = P.transform.parent;
                    P.transform.parent = CAM_3P.transform.parent;
                    Action OF3 = () => {
                        P.transform.parent = TEMP_PARENT3;
                        if (OnFinish != null) OnFinish();
                    };
                    iTween.MoveTo(CAM_3P.gameObject, P.transform.localPosition, Vel, true, OF3, "MOVE");
                    iTween.RotateTo(CAM_3P.gameObject, P.transform.rotation.eulerAngles, Vel, false, null, "ROT");
                    iTween.ValueTo(CAM_3P.gameObject, CAM_3P.fieldOfView, 78.0F, Vel, (V) => { CAM_3P.fieldOfView = (float)V; }, null, "FOV");
                    GM.i.DelayAction(0.2F, () => {
                        PlayerShipBody.SetActive(true);
                        Cockpit.SetActive(false);
                    });
                    break;
            }
        });
    }

    public void MoveTo(PT PT,Action OnFinish=null) {
        iTween.StopByName("MOVE");
        GM.i.DelayFrameAction(3, () => {
            iTween.MoveTo(gameObject, PM.i.GetPOP(PM.LayerTypes.Player, PT).transform.position, 0.3F,false, OnFinish, "MOVE");
        });
    }

    void OnTriggerEnter(Collider other) {
        if (MGL.i.GameOver) return;
        Debug.Log("Player collider detect: " + other.name);
        if (other.name.Contains("Meteor"))
        Damage();
    }
    public void Damage(int Amount = 0) {
        if (MGL.i.GameOver) return;
        Amount = Amount == 0 ? 10 : Amount;
        Debug.Log("Damage received!: " + Amount);
        CurrentEnergy -= Amount;
        AM.i.PlaySFX(SFX_MeteorCrash, AUD_PlayerShip);
        Instantiate(FX_HitDamage,transform.position,transform.rotation);
        iTween.ShakeRotation(gameObject, new Vector3(0, 0, 30), 0.6F);
        MIF.i.SetEnergy(CurrentEnergy);
        iTween.ShakeRotation(MGL.i.MainCamera, new Vector3(0, 0, 30), 0.6F);
        iTween.ShakeRotation(MIF.i.GUICamera, new Vector3(0, 0, 30), 0.6F);
        if (CurrentEnergy <= 0) {
            var CAM_ANMT = MGL.i.MainCamera.GetComponent<Animator>();
            CAM_ANMT.SetBool("ShipExplode", true);
            CAM_ANMT.enabled = true;
            Time.timeScale = 0.1F;
            Instantiate(FX_ShipDestroyed, transform.position, transform.rotation);
            Destroy(PlayerShip);
            GM.i.DelayAction(2, () => { Time.timeScale = 1.0F; });
            MGL.i.CallGameOver();
        }
    }

    internal void Shoot() {
        //throw new NotImplementedException();
    }

    public void Awake() {
        PlayerHelper.i = this;
    }

	// Use this for initialization
	void Start () {
        Debug.Log(GM.i);
        Thrusters = GM.i.SearchTransforms(transform, "Thruster_", false).ToList<Transform>().SelectMany(T => T.GetComponents<ThrusterHelper>()).ToArray<ThrusterHelper>();
	}
	
	// Update is called once per frame
	void Update () {
   
            foreach (ThrusterHelper TH in Thrusters)
                TH.ThrusterPower = ThrustersPower;
	}


}
