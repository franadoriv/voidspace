﻿using UnityEngine;
using GM = GlobalManager;
using AM = AudioManager;
using System;
using UnityEngine.EventSystems;

public class PanelBase : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {

    public CanvasRenderer CR;
    public RectTransform RT;
    public AudioClip SFX_OnOpen;
    public AudioClip SFX_OnClose;
   public  bool blocked = false;

    public virtual void OnPointerClick() { }
    public virtual void OnPointerEnter() { }
    public virtual void OnPointerExit() { }

    public void Awake() {
        CR = GetComponent<CanvasRenderer>();
        RT = GetComponent<RectTransform>();
        RT.localScale = new Vector3(0, 0, 1);
        gameObject.SetActive(false);
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Show(bool open = true, Action OnFinish = null) {
        gameObject.SetActive(true);
        iTween.StopByName("SIZE" + GetInstanceID());
        GM.i.DelayFrameAction(3, () => {
            AM.i.PlaySFX(open ? SFX_OnOpen:SFX_OnClose);
            iTween.ValueTo(gameObject, open ? 0.0F : 1.0F, open ?1.0F : 0.0F, 0.350F, (V) => {
                RT.localScale = new Vector3((float)V, 1, 1);
            }, OnFinish, "SIZE"+GetInstanceID(),easetype:iTween.EaseType.easeOutBounce);
        });
    }

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData) { OnPointerClick(); }

    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData) { OnPointerEnter(); }

    void IPointerExitHandler.OnPointerExit(PointerEventData eventData) { OnPointerExit(); }

}
