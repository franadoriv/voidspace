﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.UI;
using AM = AudioManager;
using MGL = MGL_MainGame;

public class NPCHelper : MonoBehaviour {

    public Vector3 RotationVelocity = new Vector3(0.5f,0.5F,0.5F);
    public NPCT Type;
    public PositionMatrix.PositionTypes PositionType;
    public Transform[] Trayectory = null;
    public float PathCompletion = 0.0F;
    public float Velocity = 0.0F;
    public bool StopAtEnd = false;
    Queue<SplineSegment> pathSegments;
    public enum NPCT{Meteor,Ship }
    public AudioClip SFX_OnDestroy;
    public GameObject FX_OnDestroy;
    public AudioClip SFX_OnCharge;
    int nextPoint = 0;
    public Image TEX_Time4Attack;
    public bool T4AP = true;
    public bool Dead = false;
    public Vector3 TEX_Time4Attack_POS;
    public WSP_HighRezLaser Laser;
    class SplineSegment {
        public float StartPercentage;
        public float EndPercentage;
        public float Length;
        public float Distortion;
    }

    

    void Tween() {
     Vector3 toPoint = Trayectory[nextPoint].position;
     float distance = Vector3.Distance(toPoint, transform.position);
     float FV = distance / Velocity;
     iTween.MoveTo(gameObject, iTween.Hash("position", toPoint, "time", FV, "easetype", "linear", "oncomplete", "Complete", "islocal",false));
 }

    public void LoadTime4AttackTEX() {
        //TEX_Time4Attack = Instantiate(TEX_Time4Attack.gameObject, transform.position, transform.localRotation, transform).GetComponent<Image>();
        TEX_Time4Attack.transform.parent = GameObject.Find("3DLocalCanvas").transform;
        TEX_Time4Attack.name = "TEX_T4A_" + GetInstanceID();
        TEX_Time4Attack.transform.localRotation = new Quaternion();
    }

    public void Complete() {
        nextPoint++;
        if (!StopAtEnd) {
            if (nextPoint < Trayectory.Length) Tween();
            else Destroy(gameObject);
        }
        else if (nextPoint < Trayectory.Length - 2) Tween();
        else {
            TEX_Time4Attack.fillAmount = 0.0F;
            TEX_Time4Attack.gameObject.SetActive(true);
            AM.i.PlaySFX(SFX_OnCharge);
            iTween.ValueTo(gameObject, 0.0F, 1.0F, 2.0F, (V) => {
                TEX_Time4Attack.fillAmount = (float)V;
            },()=> {
                Attack();
            });
        }
    }

    public void Attack() {
        Laser.CurrentTarget = PlayerHelper.i.transform;
        Laser.FireLaser();
    }

    public void MoveThrough(Transform[] trayectory) {
        Trayectory = trayectory;
        var arrayt = new List<Vector3>();
        foreach (var tp in trayectory) arrayt.Add(tp.position);
        var array = arrayt.ToArray<Vector3>();
        var pathLength = iTween.PathLength(array);
        var normalisedSegmentLength = pathLength / (array.Length - 1);
        pathSegments = new Queue<SplineSegment>();
        for (int i = 0; i < (array.Length - 1); i++) {
            var float_i = (float)i;
            var segmentLength = iTween.PathLength(new Vector3[] { array[i], array[i + 1] });
            pathSegments.Enqueue(new SplineSegment() {
                Length = segmentLength,
                StartPercentage = (float_i / (array.Length - 1)),
                EndPercentage = ((float_i + 1) / (array.Length - 1)),
                Distortion = normalisedSegmentLength / segmentLength
            });
        }


        //----------------
        //Debug.Log("Path points: " + trayectory.Length);
        //PathCompletion = 0.0F;
        Tween();
        //StartCoroutine(StartTrayectory());
    }

    IEnumerator StartTrayectory() {
        if (Trayectory != null && Trayectory.Length > 2) {
            while (PathCompletion < 1.0F
                ) {
                    if (PathCompletion > pathSegments.Peek().EndPercentage) pathSegments.Dequeue();
                    PathCompletion += Time.deltaTime * Velocity * pathSegments.Peek().Distortion;
                    iTween.PutOnPath(gameObject, Trayectory, PathCompletion);
                yield return new WaitForFixedUpdate();
            }
        }
        Destroy(gameObject);
    }

    void OnMouseEnter() {
        if (MGL.i.CurrentGameMode!= MGL.GameModes.Attacking || MGL.i.GameOver) return;
        CursorManager.i.SetCursor(CursorManager.CursorType.Targeted, true);
    }

    void OnMouseDown() {
        if (MGL.i.CurrentGameMode != MGL.GameModes.Attacking || MGL.i.GameOver) return;
        if (PlayerHelper.i.Laser.LaserFiring)return;
        PlayerHelper.i.Laser.CurrentTarget = this.transform;
        PlayerHelper.i.Laser.LaserFiring = true;
        GlobalManager.i.DelayAction(0.2F, () => {
            PlayerHelper.i.Laser.LaserFiring = false;
            if (FX_OnDestroy) {
               var FX = (GameObject)Instantiate(FX_OnDestroy, transform.position, transform.rotation);
               var AS = FX.AddComponent<AudioSource>();
               AS.minDistance = 4;
               AS.maxDistance = 5;
               AS.spatialBlend = 1.0F;
               AM.i.PlaySFX(SFX_OnDestroy, AS);
               Destroy(FX, 3);
            }
            Dead = true;
            Destroy(gameObject);
            if (PositionType != null)
                PositionMatrix.i.ReservedRoutes.Remove(PositionType);
        });
    }

    void OnMouseExit() {
        if (MGL.i.CurrentGameMode != MGL.GameModes.Attacking || MGL.i.GameOver) return;
        CursorManager.i.SetCursor(CursorManager.CursorType.Bullseye, true);
    }
 
    void OnDestroy() {
        if (MGL.i.CurrentGameMode != MGL.GameModes.Attacking || !Dead || MGL.i.GameOver) return;
        CursorManager.i.SetCursor(CursorManager.CursorType.Bullseye, true);
        if (TEX_Time4Attack)
            Destroy(TEX_Time4Attack);
    }

	// Use this for initialization
	void Start () {
        name = name + " - " + GetInstanceID();
        if (Type == NPCT.Meteor) {
            RotationVelocity.x = Random.Range(-0.5F, 0.5F);
            RotationVelocity.y = Random.Range(-0.5F, 0.5F);
            RotationVelocity.z = Random.Range(-0.5F, 0.5F);
        }
        else if (Type == NPCT.Ship) {
            StopAtEnd = true;
            LoadTime4AttackTEX();
        }
            

        Velocity = 30.0F;
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(RotationVelocity);
        if (TEX_Time4Attack && T4AP) {
            var P = transform.position;
            //P.z += 0.5F;
            P.x -= 1;
            P.y += 2;
            TEX_Time4Attack.transform.position = P;
        }
        if (TEX_Time4Attack)
            TEX_Time4Attack_POS = TEX_Time4Attack.transform.position;

        if (MGL.i.GameOver)
            DestroyImmediate(gameObject);

    }
}
