﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MIF = MainInterfaceManager;
using GM = GlobalManager;
using MIN = MainInputManager;

public class DancePadHelper : MonoBehaviour {

    MainSceneCode MSC;
    UIWidget MonstersIconsPanel; 
    List<GameObject> MonstersIcons = new List<GameObject>();

    public void SetPanelVisibility(bool Visible) {
        MonstersIconsPanel.gameObject.SetActive(Visible);
    }
    /*
    public void SetMonsterIconVisibility(MonsterManager.SidePostion SidePosition, bool Visible) {
        if (MonstersIcons.Count == 0) return;
        MonstersIcons.Find(MI => MI.name.Contains(SidePosition.ToString()));
    }

    public void ClearAnimations(MonsterManager.SidePostion CurrentSidePosition) { 
        ClearAnimations(MonstersIcons.Find(MI => MI.name.Contains(CurrentSidePosition.ToString())));
    }
    */
    void ClearAnimations(GameObject MI) {
        MI.GetComponent<TweenRotation>().ResetToBeginning();
        MI.GetComponent<TweenScale>().ResetToBeginning();
        MI.GetComponent<TweenColor>().ResetToBeginning();
    }
    /*
    public void HideIcon(MonsterManager.SidePostion CurrentSidePosition) {
        HideIcon(MonstersIcons.Find(MI => MI.name.Contains(CurrentSidePosition.ToString())));
    }
    */
    void HideIcon(GameObject MI) {
        ClearAnimations(MI);
        GM.i.DelayFrameAction(1, () => { MI.SetActive(false); });
    }

    public void ClearMonsterIconsPanel() {
        MonstersIcons.ForEach(MI => HideIcon(MI));
    }
    /*
    public void MonsterIconsShow(List<MonsterHelper> MHL) {
        ClearMonsterIconsPanel();
        GM.i.DelayFrameAction(2, () => {
            MHL.ForEach(M => MonstersIcons.Find(MI => MI.name.Contains(M.CurrentSidePosition.ToString())).gameObject.SetActive(true));
        }); 
    }

    public void MonsterIconsShow(List<MonsterManager.SidePostion> SidePositions) {
        ClearMonsterIconsPanel();
        SidePositions.ForEach(P => MonstersIcons.Find(MI => MI.name.Contains(P.ToString())).gameObject.SetActive(true));
    }

    public void MonsterIconAdvice(MonsterManager.SidePostion SidePosition) {
        if (MonstersIcons.Count == 0) return;
        MonstersIcons.ForEach(MI =>ClearAnimations(MI));
        GM.i.DelayFrameAction(1, () => {
            var MIR = MonstersIcons.Find(MI => MI.name.Contains(SidePosition.ToString()));
            MIR.GetComponent<TweenScale>().PlayForward();
            MIR.GetComponent<TweenColor>().PlayForward();
        });
    }

    public void MonsterIconHit(MonsterManager.SidePostion SidePosition) {
        if (MonstersIcons.Count == 0) return;
        var TR = MonstersIcons.Find(MI => MI.name.Contains(SidePosition.ToString())).GetComponent<TweenRotation>();
        TR.ResetToBeginning();
        GM.i.DelayFrameAction(1, () => {
            TR.GetComponent<TweenRotation>().PlayForward();
        });
       
        
    }

    public void MonsterIconReturnNormal(MonsterManager.SidePostion Position) {
        if (MonstersIcons.Count == 0) return;
        var MIC = MonstersIcons.Find(MI => MI.name.Contains(Position.ToString()));
        MIC.GetComponent<TweenRotation>().ResetToBeginning();
        MIC.GetComponent<TweenScale>().ResetToBeginning();
        MIC.GetComponent<TweenColor>().ResetToBeginning();
    }
    */
	// Use this for initialization
	void Start () {
        var MSC_GO = GameObject.Find("MainSceneCode");
        if(MSC_GO!=null){
            MSC = MSC_GO.GetComponent<MainSceneCode>();
            GM.i.SearchTransforms(MIF.i.transform,"TEX_DH_MI",false).ToList().ForEach(X => MonstersIcons.Add(X.gameObject));
            MonstersIconsPanel = GM.i.SearchTransform(MIF.i.transform, "CNT_DancePadHelper").GetComponent<UIWidget>();
            ClearMonsterIconsPanel();
            Debug.Log("MSC.MIN.GamePadDetected: " + MIN.i.GamePadDetected);
            SetPanelVisibility(MIN.i.GamePadDetected);
        }
       
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
