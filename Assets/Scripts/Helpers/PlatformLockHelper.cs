﻿using UnityEngine;
using System.Collections;
using GM = GlobalManager;

public class PlatformLockHelper : MonoBehaviour {

    public AudioClip SFX_OnReleaseLock;
    MainSceneCode MSC;
    void ReleaseLock() { 
        //AM.i.PlaySFX(SFX_OnReleaseLock);
        GetComponent<AudioSource>().PlayOneShot(SFX_OnReleaseLock);
        foreach(Transform T in GM.i.SearchTransforms(transform,"ShipPlatformLock_FX")){
            T.gameObject.SetActive(true);
            GM.i.DelayAction(1, () => { Destroy(T.gameObject); });
        }
    }

	// Use this for initialization
	void Start () {
        MSC = GameObject.Find("MainSceneCode").GetComponent<MainSceneCode>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
