﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System;
using UE = UnityEngine;
using GM = GlobalManager;
using MGL = MGL_MainGame;

public class PositionMatrix : MonoBehaviour {

    public static PositionMatrix i;

    MainSceneCode MSC;

    public enum LayerTypes { Behid,Player, Middle, Far,Other }

    public enum PositionTypes { NW, N, NE, W,C,E,SW,S,SE,CAM_3P }

    public PointOfPosition[] Positions;

    public List<PositionTypes> ReservedRoutes = new List<PositionTypes>();

    public PointOfPosition GetPOP(LayerTypes LayerType, PositionTypes PositionType) {
        return Positions.Where(P => P.LayerType == LayerType && P.PositionType == PositionType).FirstOrDefault();
    }

    public Transform[] GetLinearTrayectory(PositionTypes PositionType) {
        Transform[] R = new Transform[5];
                R[0] = MGL.i.GeneralSpawnPoint.transform;
                R[1] = GetPOP(LayerTypes.Far, PositionType).transform;
                R[2] = GetPOP(LayerTypes.Middle, PositionType).transform;
                R[3] = GetPOP(LayerTypes.Player, PositionType).transform;
                R[4] = GetPOP(LayerTypes.Behid, PositionType).transform;
        return R;
    }

    public Transform[] GetTrayectory() {
        var Route = (PositionTypes)UE.Random.Range(0, 9);
        return GetLinearTrayectory(Route);
    }

    public Transform[] GetRandomTrayectory(bool ReservationMode = false) {
        var Route = (PositionTypes)UE.Random.Range(0, 9);
        if (ReservationMode) {
            while (ReservedRoutes.Contains(Route)) 
                Route = (PositionTypes)UE.Random.Range(0, 9);
            ReservedRoutes.Add(Route);
            return GetLinearTrayectory(Route);
        }
        return GetLinearTrayectory(Route);

    }




    void Awake() {
        i = this;
        MSC = GameObject.Find("MainSceneCode").GetComponent<MainSceneCode>();

    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
