﻿using UnityEngine;
using System.Collections;

public class FixedTransformHelper : MonoBehaviour {
    public Quaternion fixedRotation;
    public Vector3 VfixedRotation;
	// Use this for initialization
	void Awake () {
        fixedRotation = transform.rotation;
        VfixedRotation = transform.eulerAngles;
	}
	
	// Update is called once per frame
    void LateUpdate() {
        //fixedRotation.y = transform.rotation.y;
        //transform.rotation = fixedRotation;
        //var ParentYvalue = transform.parent.eulerAngles.y;
        VfixedRotation.y = transform.parent.eulerAngles.y-90;
        transform.eulerAngles = VfixedRotation; 
        //var rotation = fixedRotation;
        
	}
}
