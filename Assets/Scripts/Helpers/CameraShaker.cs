﻿using UnityEngine;
using System.Collections;

public class CameraShaker : MonoBehaviour {

    Vector3 origin;
    Vector3 target;
    float ratio = 0.01f;
    void Start() {
        origin = transform.localPosition;
        InvokeRepeating("ChangeTarget", 0.01f, 0.5f);
    }
    void Update() {
        transform.localPosition = Vector3.Lerp(transform.localPosition, target, ratio);
    }
    void ChangeTarget() {
        float x = Random.Range(-1.0f, 1.0f);
        float y = Random.Range(-1.0f, 1.0f);
        target = new Vector3(origin.x + x, origin.y + y, origin.z);
    }
}
