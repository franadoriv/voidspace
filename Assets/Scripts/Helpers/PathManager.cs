﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class PathManager : MonoBehaviour {
    public Transform[] Points;
	// Use this for initialization
	void Start () {
        if (Points != null) {
            List<Vector3> PS = new List<Vector3>();
            PS.Add(new Vector3());
            foreach (Transform P in Points) { 
                PS.Add(P.position);
                Debug.Log(P.position);
            }
            PS.Add(new Vector3());
            var S = GameObject.Find("Sphere");
            Debug.Log(PS.Count);
            LeanTween.moveSpline(S,PS.ToArray<Vector3>(), 5.0f).setEase(LeanTweenType.linear).setOrientToPath(true);
        }
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
