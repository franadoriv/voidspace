﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using GM = GlobalManager;
using AM = AudioManager;
using UnityEngine.UI;
using MIN = MainInputManager;

public class MainInterfaceManager : MonoBehaviour {

    public static MainInterfaceManager i;

    public Canvas LocalCanvas;

    //Functions
    public void ChangeToPanel(GameObject To, Action OnFinish = null) {
        GameObject Actual;
        var ActualT = GM.i.SearchTransforms(MIN.i.transform, "PNL_", false, false).ToList().FirstOrDefault();
        Actual = ActualT != null ? ActualT.gameObject : null;
        ChangeToPanel(Actual, To, OnFinish); 
    }

    public void ChangeToPanel(GameObject Actual, GameObject To, Action OnFinish = null) {
       var ToAnm = To.GetComponent<UITweener>();
       if (Actual != null) {
           var ActAnm = Actual.GetComponent<UITweener>();
           var ActOF = new EventDelegate(() => {
               Actual.SetActive(false);
               To.SetActive(true);
               if (OnFinish != null) {
                   var ToOF = new EventDelegate(() => {
                       OnFinish();
                   });
                   ToOF.oneShot = true;
                   ToAnm.onFinished.Add(ToOF);
                   ToAnm.PlayForward();
               }
               ToAnm.PlayForward();
           });
           ActOF.oneShot = true;
           ActAnm.onFinished.Add(ActOF);
           ActAnm.PlayReverse();
       }
       else{
           To.SetActive(true);
           if (OnFinish != null) {
               var ToOF = new EventDelegate(() => {
                   OnFinish();
               });
               ToOF.oneShot = true;
               ToAnm.onFinished.Add(ToOF);
               ToAnm.PlayForward();
           }
           ToAnm.PlayForward();
       }
    }
    
    public void ChangeToPanelByName(string To,Action OnFinish=null) {
        ChangeToPanel(GM.i.SearchTransform(transform, To).gameObject, OnFinish);
    }

    public void ChangePanelByName(string To) {
        ChangePanel(null, GM.i.SearchTransform(transform, To).gameObject);
    }
    public void ChangePanel(GameObject From, GameObject To) {
        var T2 = To.GetComponent<UITweener>();
        if (From != null && To != null) {
            var T1 = From.GetComponent<UITweener>();
            var Z = new EventDelegate(() => {
                From.gameObject.SetActive(false);
                To.gameObject.SetActive(true);
                T2.PlayForward();
            });
            Z.oneShot = true;
            T1.AddOnFinished(Z);
            T1.PlayReverse();
        }
        else {
            To.gameObject.SetActive(true);
            T2.PlayForward();
        }
    }

    public void FadeInPanel(string PanelName, Action OnFinish = null,float seconds=1) {
        var Panel = GM.i.SearchTransform(transform,PanelName);
        var PanelUI = Panel.GetComponent<UIPanel>();
        PanelUI.alpha = 0;
        Panel.gameObject.SetActive(true);
        iTween.Stop(Panel.gameObject);
        GM.i.DelayFrameAction(2, () =>{
            iTween.ValueTo(Panel.gameObject, 0, 1, seconds, (V) =>{
                PanelUI.alpha = (float)V;
            }, OnFinish);
        });
    }

    public void FadeOutPanel(string PanelName, Action OnFinish = null, float seconds = 1) {
        var Panel = GM.i.SearchTransform(transform, PanelName);
        var PanelUI = Panel.GetComponent<UIPanel>();
        PanelUI.alpha = 1;
        iTween.Stop(Panel.gameObject);
        GM.i.DelayFrameAction(2, () => {
            iTween.ValueTo(Panel.gameObject, 1, 0, seconds, (V) => {
                PanelUI.alpha = (float)V;
            }, () => { 
                Panel.gameObject.SetActive(true);
                if(OnFinish!=null)
                    OnFinish(); 
            });
        });
    }

    public void ClosePanel(GameObject Panel, Action OnFinish = null) {
        var T = Panel.GetComponent<UITweener>();
        var Z = new EventDelegate(() => {
            Panel.gameObject.SetActive(false);
            if (OnFinish != null)
                OnFinish();
        });
        Z.oneShot = true;
        T.AddOnFinished(Z);
        T.PlayReverse();
    }

    public void Panel(string Name, bool open = true,  Action OnFinish = null,AudioClip SFX_OnOpen=null,AudioClip SFX_OnClose=null) {
        var PNL = LocalCanvas.GetComponentsInChildren<Transform>(true).Where(P => P.name == Name).FirstOrDefault().gameObject;
        PNL.SetActive(true);
        iTween.StopByName("SIZE" + GetInstanceID());
        GM.i.DelayFrameAction(3, () => {
            AM.i.PlaySFX(open ? SFX_OnOpen : SFX_OnClose);
            iTween.ValueTo(gameObject, open ? 0.0F : 1.0F, open ? 1.0F : 0.0F, 0.350F, (V) => {
                PNL.transform.localScale = new Vector3((float)V, 1, 1);
            }, OnFinish, "SIZE" + GetInstanceID(), easetype: iTween.EaseType.easeOutBounce);
        });
        if(!open)
        GM.i.DelayAction(0.4F, () => {
            PNL.SetActive(false);
        });
    }

	// Use this for initialization
	void Awake () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
