﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using USM = UnityEngine.SceneManagement.SceneManager;
using GM = GlobalManager;
using LM = LocalizationManager;


public class MIF_MainGame : MainInterfaceManager {

    public static MIF_MainGame i;

    ProgressBar.ProgressRadialBehaviour WDG_EnergyBar;
    public Text GameModeTittle;

    public GameObject WDG_Energy_RotationCyrcle;
    public Material[] MAT_WDG_Energy;
    public Image[] IMG_WDG_Energy;
    public Text TXT_WDG_Energy;
    public GameObject GUICamera;

    string TXT_E_MODE = "";
    string TXT_A_MODE = "";
    string TXT_H_MODE = "";


    public void SetEnergy(int Energy) {
        WDG_EnergyBar.Value = Energy;
    }

    public void SetEnergyAlpha(float Alpha) {
        foreach (Renderer R in WDG_Energy_RotationCyrcle.GetComponentsInChildren<Renderer>()) {
            R.material.SetColor("_Color", new Color(1, 1, 1, Alpha));
            foreach (Material M in R.materials) {
                if (M.HasProperty("_TintColor")) {
                    M.SetColor("_TintColor", new Color(1.0F, 1.0F, 1.0F, Alpha));
                    //var C = M.color; C.a = Alpha; M.color = C;
                }
            }
        }

        foreach (var I in IMG_WDG_Energy) {
            var C = I.color; C.a = Alpha; I.color = C;
        }

        Color C2 = TXT_WDG_Energy.color; C2.a = Alpha; TXT_WDG_Energy.color = C2;
    }

    IEnumerator Write(string NewText) {
        GameModeTittle.text = "";
        while (GameModeTittle.text.Length < NewText.Length) {
            GameModeTittle.text += NewText.Substring(GameModeTittle.text.Length, 1);
            yield return new WaitForSeconds(0.025f);
        }
        yield return new WaitForSeconds(2);
        while (GameModeTittle.text.Length > 0) {
            GameModeTittle.text = GameModeTittle.text.Substring(0, GameModeTittle.text.Length - 1);
            yield return new WaitForSeconds(0.025f);
        }
    }

    public void ShowGameMode(MGL_MainGame.GameModes GameMode,Action OnFinish=null){
        Debug.Log("ShowGameMode: " + GameMode.ToString());
        string TXT_GM = "";
        switch (GameMode) {
            case MGL_MainGame.GameModes.Evasion: TXT_GM = TXT_E_MODE; break;
            case MGL_MainGame.GameModes.Attacking: TXT_GM = TXT_A_MODE; break;
            case MGL_MainGame.GameModes.Hacking: TXT_GM = TXT_H_MODE; break;
        }
        StartCoroutine(Write(TXT_GM));
        if (OnFinish != null) 
            GM.i.DelayAction(3, OnFinish);
    }

    public void ShowInterface(bool FadeIn = true) {
        iTween.ValueTo(gameObject, FadeIn?0.0F:1.0F, FadeIn?1.0F:0.0F, 2.0F, (V) => {
            SetEnergyAlpha((float)V);
        });
    
    }

    public void BTNClick(GameObject BTN) {
        switch (BTN.name) {
            case "BTN_TryAgain_Yes":
                Panel("PNL_TryAgain", false, () => {USM.LoadScene(USM.GetActiveScene().name);});
                break;
            case "BTN_TryAgain_No":
                Panel("PNL_TryAgain", false, () => { USM.LoadScene(0); });
                break;
        }
    }

    void Awake() {
        i = this;
        WDG_EnergyBar = GameObject.Find("WDG_Energy").GetComponent<ProgressBar.ProgressRadialBehaviour>();
        
    }

	// Use this for initialization
	void Start () {
        SetEnergyAlpha(0.0F);
        LM.i.GetText("TXT_E_MODE", "EVASION MODE ACTIVATED", (STR) => { TXT_E_MODE = STR; });
        LM.i.GetText("TXT_A_MODE", "ATTACKING MODE ACTIVATED", (STR) => { TXT_A_MODE = STR; });
        LM.i.GetText("TXT_H_MODE", "HACKING MODE ACTIVATED", (STR) => { TXT_H_MODE = STR; });


    }
	
	// Update is called once per frame
	void Update () {

	}
}
