﻿using UnityEngine;
using System.Collections;
using LM = LocalizationManager;
using UnityEngine.UI;
using System;
using GM = GlobalManager;
using MSC = MSC_HangarIntro;

public class MIF_HangarIntro : MainInterfaceManager{

    public Text LBL_MISSION_OBJECTIVES;
    public Text LBL_EM_TITLE;
    public Text LBL_EM_EXPLAIN;
    public Text LBL_AM_TITLE;
    public Text LBL_AM_EXPLAIN;
    public Text LBL_HM_TITLE;
    public Text LBL_HM_EXPLAIN;
    public Text LBL_START;
    public Text LBL_PRESS_ANY_KEY;

    // Use this for initialization
    void Start () {
        LM.i.FillLabel("MISSION_OBJECTIVES", LBL_MISSION_OBJECTIVES, "MISSION OBJECTIVES");
        LM.i.FillLabel("EM_TITLE", LBL_EM_TITLE, "EVASSION MODE");
        LM.i.FillLabel("EM_EXPLAIN", LBL_EM_EXPLAIN, "Before approaching enemy colonies, we must go through different kinds of space debris that my be hazardous to the shields. Use the arrow keys on your keyboard or DancePad to avoid these objects.");
        LM.i.FillLabel("AM_TITLE", LBL_AM_TITLE, "ATTACK MODE");
        LM.i.FillLabel("AM_EXPLAIN", LBL_AM_EXPLAIN, "When you get near the enemy colony, assault ships will come out to attack you. Use the mouse to shoot them with your high impact laser or press the arrow of your DancePad according to the position of the enemy.");
        LM.i.FillLabel("HM_TITLE", LBL_HM_TITLE, "HACK MODE");
        LM.i.FillLabel("HM_EXPLAIN", LBL_HM_EXPLAIN, "After clean the first wave defense, your ship will should be inside of their main signal range. Choose the answer correctly, disable their defenses and your fleet will take care of them.");
        LM.i.FillLabel("START", LBL_START, "START");
        LM.i.FillLabel("PRES_ANY_KEY_TO_SKIP", LBL_PRESS_ANY_KEY, "Press any key to skip");
        BlinkPAK();
        GM.i.DelayAction(20, () => {
            BlinkPAK(true);
            MSC.i.CanBeenSkiped = false;
        });
    }

    public void FadePAK() {
        var HT = new Hashtable();
        HT.Add("from", 0.0F);
        HT.Add("to", 1.0F);
        HT.Add("time", 1.0F);
        HT.Add("looptype", iTween.LoopType.pingPong);
        Action<object> OnUpdate = (v) => {
            var C = LBL_PRESS_ANY_KEY.color;
            C.a = (float)v; LBL_PRESS_ANY_KEY.color = C;
        };
        HT.Add("onupdate", OnUpdate);
        HT.Add("name", "PAK_BLINK");
        iTween.ValueTo(LBL_PRESS_ANY_KEY.gameObject, HT);
    }

    public void BlinkPAK(bool Stop = false) {
        if (!LBL_PRESS_ANY_KEY.gameObject) return;
        iTween.StopByName(LBL_PRESS_ANY_KEY.gameObject, "PAK_BLINK");

        if (!Stop) FadePAK();
        else iTween.ValueTo(LBL_PRESS_ANY_KEY.gameObject, LBL_PRESS_ANY_KEY.color.a,0.0F,0.3F,(v)=>{
            var C = LBL_PRESS_ANY_KEY.color;
            C.a = (float)v; LBL_PRESS_ANY_KEY.color = C;
        },null,"PAK_BLINK");
    }
	
	// Update is called once per frame
	void Update () {
	
	}


}
