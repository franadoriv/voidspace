﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GM = GlobalManager;
using SM = SceneManager;
using MSC = MSC_Ending;
using AM = AudioManager;
using LM = LocalizationManager;

public class MIF_Ending : MainInterfaceManager {

    public static MIF_Ending i;

    public Text LBL_PressAnyKey;
    public Text LBL_Congratulations;
    public AudioClip SFX_OnAnyPressKey;

    Coroutine PressAnyKeyBounce;

    IEnumerator LoopFade(MaskableGraphic g, float TimeSeconds = 2.5F) {
        while (true) {
            Debug.Log("LoopFade");
            g.CrossFadeAlpha(0, TimeSeconds / 2, true);
            while (g.GetComponentsInChildren<CanvasRenderer>()[0].GetAlpha() > 0.0F) {
                yield return new WaitForSeconds(1);
            }
            g.CrossFadeAlpha(1, TimeSeconds / 2, true);
            while (g.GetComponentsInChildren<CanvasRenderer>()[0].GetAlpha() < 1.0F) {
                yield return new WaitForSeconds(1);
            }
            yield return new WaitForSeconds(1);
        }
    }

    public void ShowCongratulations() {
        iTween.ValueTo(LBL_Congratulations.gameObject, 0.0F, 1.0F, 2.0F, (V) => {
            var C = LBL_Congratulations.color; C.a = (float)V; LBL_Congratulations.color = C;
        });
    }

    public void ShowPressAnyKey() {
        MSC.i.CanBeReset = true;
        LBL_PressAnyKey.gameObject.SetActive(true);
        PressAnyKeyBounce = StartCoroutine(LoopFade(LBL_PressAnyKey));
    }

    public void FadeAllTexts() {
        AM.i.PlaySFX(SFX_OnAnyPressKey);
        float a = 0.0F;
        for (a = 0.0F; a < 1.0F; a += 0.01F) {
            SetAlpha(LBL_PressAnyKey, a);
            SetAlpha(LBL_Congratulations, a);
        }
    }

    void Awake() { i = this; }

    void SetAlpha(Text T,float a) {
        var C = T.color; C.a = a; T.color = C;
    }

    // Use this for initialization
    void Start () {
        LM.i.FillLabel("CONGRATULATIONS", LBL_Congratulations, "CONGRATULATIONS");
        LM.i.FillLabel("PRESS_ANY_KEY", LBL_PressAnyKey, "PRESS ANY KEY");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
