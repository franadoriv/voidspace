﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using MIF = MIF_MainGame;
using GM = GlobalManager;
using EMS = EnemyMotherShipController;
using AM = AudioManager;
using MSC = MSC_MainGame;
using PH = PlayerHelper;
using GCC = GameCameraController;
using CM = CursorManager;
using PM = PositionMatrix;
using GCM = GlobalCanvasManager;
using ALTM = AlertManager;
using SM = SceneManager;
using MIN = MIN_MainGame;
using ASM = AudioStreamManager;
using LM = LocalizationManager;
using MGL = MGL_MainGame;


public class MGL_MainGame : MainGameLogic {

    public static MGL_MainGame i;

    public GameObject RouteFollower;
    public GameObject MeteorPrefab;
    public GameObject EnemyPrefab;
    public List<GameObject> CurrentMeteorList;
    public Transform[] MeteorDemoPath;
    public GameObject GeneralSpawnPoint;
    public GameObject MainCamera;
    public PH Player;
    public bool GameOver = false;
    public int EnemiesCreated = 0;
    public int MaxEnemiesCreation = 10;
    string TXT_TRY_AGAIN;

    //VFX
    public F3DWarpJump VFX_JumpIn;
    public F3DWarpJump VFX_JumpOut;

    //SFX
    public AudioClip SFX_EvasionModeActivated;
    public AudioClip SFX_AttackingModeActivated;
    public AudioClip SFX_HackingModeActivated;
    public AudioClip SFX_TurnOffScreen;


    public GameModes CurrentGameMode = GameModes.Cutscene;

    public enum GameModes {Cutscene,Evasion,Attacking,Hacking}

    public void ClearDebugComponents(){ }

    public void InitIntro() {
        AM.i.PlayBGM(MSC.i.BGM_Main);
        ASM.i.Play("BGM_InGame", () => {
            GCM.i.Fade(true, () => {
                GM.i.DelayAction(1, () => { InitGame(); });
            });
        });
    }

    public void InitGame() {

        GM.i.DelayAction(2, () => { ChangeGameMode(GameModes.Evasion); });
        GM.i.DelayAction(30, () => { ChangeGameMode(GameModes.Attacking); });
    }

    public void JumpIn(Action OnFinish) {
        //PH.i.PlayerShip.pare
    }

    public void ChangeGameMode(GameModes NewGameMode) {
        if (GameOver) return;
        CurrentGameMode = NewGameMode;
        Debug.Log("GameMode " + NewGameMode.ToString() + " selected");
        CM.i.SetCursor(CM.CursorType.Blocked);
        MIN.i.InputEnabled = false;
        switch (NewGameMode) {
            case GameModes.Evasion:
                MIF.i.ShowGameMode(CurrentGameMode);
                Player.ChangeView(PH.ViewTypes.outside);
                MIF.i.ShowInterface();
                AM.i.PlaySFX(SFX_EvasionModeActivated);
                GM.i.DelayAction(1, () => {
                    MIF.i.SetEnergy(100);
                    StartCoroutine(MeteorCreationCycle());
                    MIN.i.InputEnabled = true;
                });
                break;
            case GameModes.Attacking:
                MIF.i.ShowGameMode(CurrentGameMode);
                CM.i.SetCursor(CM.CursorType.Bullseye, true);
                Player.ChangeView(PH.ViewTypes.outside);
                MIF.i.ShowInterface();
                AM.i.PlaySFX(SFX_AttackingModeActivated);
                PH.i.MoveTo(PM.PositionTypes.S, () => {
                    MIF.i.SetEnergy(100);
                    StartCoroutine(EnemyCreationCycle(() => {
                      ChangeGameMode(GameModes.Hacking);
                        MIN.i.InputEnabled = true;
                    }));
                });
                break;
            case GameModes.Hacking:
                EMS.i.Appear(() => {
                    MIF.i.ShowInterface(false);
                    PH.i.MoveTo(PM.PositionTypes.S, () => { 

                    });
                    Player.ChangeView(PH.ViewTypes.cocktip, () => {
                        MIF.i.ShowGameMode(CurrentGameMode);
                        CM.i.SetCursor(CM.CursorType.Hand);
                        AM.i.PlaySFX(SFX_HackingModeActivated);
                        Player.ChangeView(PH.ViewTypes.cocktipfront, () => {
                            QAPanelController.i.Load();
                        });
                    });
                });
                break;
        }
    }

    public void NextPhase() { }

    public void CallGameOver(bool Fail = true) {
        GameOver = true;
        MGL.i.CurrentGameMode = GameModes.Cutscene;
        if (Fail) {
            GCC.i.BlackAndWhite(true, () => {
                GM.i.DelayAction(1, () => {
                    // AM.i.PlayBGM(MSC.i.BGM_GameOver);
                    ASM.i.Play("BGM_GameOver", () => {
                        iTween.ValueTo(PH.i.CAM_3P.gameObject, 1.0F, 0.1F, 1.5F, (V) => {
                            Rect RV = new Rect(PH.i.CAM_3P.rect.x, (1.0F - (float)V) / 2, PH.i.CAM_3P.rect.width, (float)V);
                            PH.i.CAM_3P.rect = RV;
                            PH.i.CAM_SPC.rect = RV;
                        }, null, "GOV");
                        GM.i.DelayAction(1, () => {
                            AM.i.PlaySFX(SFX_TurnOffScreen);
                            iTween.ValueTo(PH.i.CAM_3P.gameObject, 1.0F, 0.0F, 1.5F, (V) => {
                                var RH = new Rect((1.0F - (float)V) / 2, PH.i.CAM_3P.rect.y, (float)V, PH.i.CAM_3P.rect.height);
                                PH.i.CAM_3P.rect = RH;
                                PH.i.CAM_SPC.rect = RH;
                            }, null, "GOH");
                        });
                    });
                });
                GM.i.DelayAction(2.5F, () => { ALTM.i.Display(TXT_TRY_AGAIN, () => { SM.i.ChangeScene("MainGame"); }, () => { SM.i.ChangeScene("IntroGame"); }); });
            });
        }
        else SM.i.ChangeScene("Ending");
    }

    IEnumerator MeteorCreationCycle() {
        yield return new WaitForSeconds(1);
        while (CurrentGameMode == GameModes.Evasion) {
            GameObject Meteor = ((GameObject)Instantiate(MeteorPrefab, GeneralSpawnPoint.transform.position, GeneralSpawnPoint.transform.rotation));
            Meteor.GetComponent<NPCHelper>().MoveThrough(PM.i.GetTrayectory());
            AudioSource SFX = Meteor.GetComponent<AudioSource>();
            SFX.clip = AudioStreamManager.i.GetSFXLoaded("SFX_pulsating_tone");
            if (SFX.clip != null)SFX.Play();
            yield return new WaitForSeconds(1.3F);
        }
    }

    IEnumerator EnemyCreationCycle(Action OnAllDie) {
        yield return new WaitForSeconds(1);
        EnemiesCreated = 0; 
        while (CurrentGameMode == GameModes.Attacking && EnemiesCreated < MaxEnemiesCreation) {
            GameObject Enemy = ((GameObject)Instantiate(EnemyPrefab, GeneralSpawnPoint.transform.position, GeneralSpawnPoint.transform.rotation));
            Enemy.GetComponent<NPCHelper>().MoveThrough(PM.i.GetRandomTrayectory());
            AudioSource SFX = Enemy.GetComponent<AudioSource>();
            SFX.clip = AudioStreamManager.i.GetSFXLoaded("SFX_pulsating_tone");
            if (SFX.clip != null) SFX.Play();
            yield return new WaitForSeconds(1.0F);
            EnemiesCreated++;
        }
        Debug.Log("EnemyLifeDetection start");
        yield return new WaitForSeconds(2);
        while (GameObject.FindGameObjectsWithTag("Enemy").Count() > 0)
            yield return new WaitForSeconds(0.5F);
        Debug.Log("All enemies destroyed");
        OnAllDie();
    }
   

    void Awake() {
        base.Awake();
        i = this;
    }     
	// Use this for initialization
	void Start () {
        CM.i.SetCursor(CM.CursorType.Blocked);
        LM.i.GetText("LBL_TRY_AGAIN","TRY AGAIN?",(STR)=>{ TXT_TRY_AGAIN = STR; });
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
