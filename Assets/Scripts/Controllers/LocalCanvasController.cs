﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class LocalCanvasController : MonoBehaviour {

    void Awake() {
        foreach (Transform T in GetComponentsInChildren<Transform>(true).Where(C => C.transform.parent == transform))
            T.gameObject.SetActive(true);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
