﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using GM = GlobalManager;
using QM = QuestionManager;
using AM = AudioManager;
using QAOC = QAOptionController;
using EMSC = EnemyMotherShipController;
using MGL = MGL_MainGame;

public class QAPanelController : PanelBase {

    public static QAPanelController i;


     void Awake() {
        i = this;
        base.Awake();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Load() {
        Debug.Log("Loading QAPanelController");
        Show(true, () => {
            int N = 0; 
            QM.Question Q = QM.i.CurrentQuestion;
            foreach (QAOC OPT in GetComponentsInChildren<QAOC>(true)) {
                var X = OPT; var N2 = (0.25F * N); var N3 = N;
                GM.i.DelayAction(N2, () => { X.Load(Q, N3); X.blocked = false; });
                N++;
            }
        });

    }

    public void UnLoad(Action OnFinish = null)
    {
        foreach (QAOC OPT in GetComponentsInChildren<QAOC>(true))
        {
            int N = 0;
            var X = OPT; var N2 = (0.25F * N);
            GM.i.DelayAction(N2, () => { X.Show(false); });
            N++;
        }
        GM.i.DelayAction(0.5F, ()=> { Show(false); if(OnFinish!=null)OnFinish(); });
    }

    public void Answer(bool Correct) {
        UnLoad(()=> {
            GM.i.DelayAction(1, () => {
                HackingPanelController.i.ShowResult(Correct,()=> {
                    PlayerHelper.i.ChangeView(PlayerHelper.ViewTypes.outside);
                    if (Correct) 
                        EMSC.i.Die(()=> {
                            if (QM.i.NextQuestion() == null)
                                MGL.i.CallGameOver(false);
                            else
                                MGL.i.InitGame();
                        });
                    else
                        EMSC.i.Attack();
                });
            });
        });
    }
}
