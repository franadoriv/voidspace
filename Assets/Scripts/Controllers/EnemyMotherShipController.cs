﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using GM = GlobalManager;
using GCC = GameCameraController;
using AM = AudioManager;
using MGL = MGL_MainGame;

public class EnemyMotherShipController : MonoBehaviour {

    public static EnemyMotherShipController i;

    public GameObject Body;
    public GameObject ChildsContainer;

    public GameObject FX_Explosion;
    public AudioClip SFX_Explosion;
    public AudioClip SFX_VoiceOnDestroyed;
    public AudioClip SFX_OnAppear;
    public AudioClip SFX_OnChildAppear;
    public AudioClip SFX_VoiceOnAppear;
    public GameObject FX_ChildExplosion;


    bool ExplosionCycle = false;
    public Vector3 IP;
    //public List<GameObject> Childrens;
    public Dictionary<GameObject, Vector3> Childrens = new Dictionary<GameObject, Vector3>();
    public List<WSP_HighRezLaser> Lasers = new List<WSP_HighRezLaser>();

    public Material MAT_Mother;

    public void SetMotherAlpha(float alpha) {
        var C = MAT_Mother.color;
        C.a = alpha;
        MAT_Mother.color = C;
    }

    public void Appear(Action OnFinish = null) {
        SetMotherAlpha(0.0F);
        AM.i.PlaySFX(SFX_VoiceOnAppear);
        foreach (var GO in Childrens) {
            GO.Key.SetActive(false);
            GO.Key.transform.localPosition = new Vector3(GO.Value.x, GO.Value.y + 20, GO.Value.z);
        }
        transform.localPosition = new Vector3(IP.x, IP.y + 10, IP.z);
        Body.SetActive(true);
        ChildsContainer.SetActive(true);
        iTween.MoveTo(gameObject, IP, 2.0F, true, null, "APPEARING");
        Fade();
        AM.i.PlaySFX(SFX_OnAppear);
        GM.i.DelayAction(1.0F, () => {
            float N = 0;
            foreach (var GO in Childrens) {
                GO.Key.SetActive(true);
                //GM.i.DelayAction(0.5F+N, () => { AM.i.PlaySFX(SFX_OnChildAppear); });
                iTween.MoveTo(GO.Key, GO.Value, UnityEngine.Random.Range(0.5F, 5.0F), true, () => {
                    GO.Key.transform.parent = transform;
                });
                iTween.LookTo(GO.Key, PlayerHelper.i.transform.position, 1.0F);
                N += 0.5F;
            }
        });
        if (OnFinish != null)
            GM.i.DelayAction(Childrens.Count/2, OnFinish);
    }

    public void Fade(bool FadeIn = true,Action OnFinish = null) {
        iTween.ValueTo(gameObject, MAT_Mother.color.a, FadeIn?1.0F:0.0F, 2.0F, (V) => {
            SetMotherAlpha((float)V);
        },OnFinish);
    }

    public void Die(Action OnFinish=null, float Duration = 3){
        StartCoroutine(CreateExplosions(OnFinish, Duration));
        foreach (var Child in Childrens) {
            GM.i.DelayAction(UnityEngine.Random.Range(0.5F, 5.0F), () => {
                Destroy(Instantiate(FX_ChildExplosion, Child.Key.transform.position,Child.Key.transform.rotation),3);
                Child.Key.SetActive(false);
            });
        }
    }
    
    public void Attack() {  
        foreach (var L in Lasers)
            GM.i.DelayAction(UnityEngine.Random.Range(0.0F,5.0F),L.FireLaser);
            GM.i.DelayAction(5.0F, () => {
                Destroy(Instantiate(FX_ChildExplosion, PlayerHelper.i.transform.position, PlayerHelper.i.transform.rotation),2);
                PlayerHelper.i.PlayerShip.SetActive(false);
                foreach (var L in Lasers)
                    L.StopLaserFire();
                MGL.i.CallGameOver();
            });
    }

    IEnumerator CreateExplosions(Action OnFinish = null,float Duration = 3)
    {
        DateTime A = DateTime.Now;
        while ((DateTime.Now-A).TotalMilliseconds<Duration*1000) {
            var P = new Vector3(UnityEngine.Random.RandomRange(-5.0F, 5.0F),
                                UnityEngine.Random.RandomRange(-5.0F, 5.0F), 
                                UnityEngine.Random.RandomRange(-5.0F, 5.0F));
            P += transform.position;
            if (FX_Explosion)
                Instantiate(FX_Explosion, P,new Quaternion(),null);
            AM.i.PlaySFX(SFX_Explosion);
            
            yield return new WaitForSecondsRealtime(0.25F);
        }
        yield return new WaitForEndOfFrame();
        Fade(false, OnFinish);
        AM.i.PlaySFX(SFX_VoiceOnDestroyed);
    }

    void Awake() {
        i = this;
        MAT_Mother = GetComponentInChildren<Renderer>(true).material;
        IP = transform.localPosition;
        Lasers.AddRange(GetComponentsInChildren<WSP_HighRezLaser>(true));
        foreach (Transform T in GetComponentsInChildren<Transform>(true).Where(C => C.parent.name == "Childrens")) {
            Childrens.Add(T.gameObject, T.localPosition);
            T.gameObject.SetActive(false);
        }
        SetMotherAlpha(0);
    }

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
