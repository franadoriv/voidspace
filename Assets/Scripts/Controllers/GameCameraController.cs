﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class GameCameraController : MonoBehaviour {

    public static GameCameraController i;

    public Camera PlayerCamera;

    void Awake() {
        i = this;
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void BlackAndWhite(bool FadeIn=true,Action OnFinish = null) {
        var GSS = PlayerCamera.GetComponent<Grayscale>();
        GSS.enabled = true;
        iTween.ValueTo(PlayerCamera.gameObject, FadeIn ? 0.0F:1.0F, FadeIn?1.0F:0.0F, 1.0F, (V) => {
            GSS.effectAmount = (float)V;
        }, OnFinish);
    }

}
