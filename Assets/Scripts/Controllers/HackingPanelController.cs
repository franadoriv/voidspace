﻿
using System;
using UnityEngine;
using UnityEngine.UI;
using GM = GlobalManager;
using AM = AudioManager;
using MGL = MGL_MainGame;
using LM = LocalizationManager;

public class HackingPanelController : PanelBase {

    public static HackingPanelController i;

    public Image IMG_Background;
    public Image IMG_ProgressBar;
    public Text LBL_Percent;
    public Image SPNL_Result;
    public Text LBL_Result;
    public Text LBL_Hacking;
    public AudioClip SFX_OnHack;
    public AudioClip SFX_OnCorrect;
    public AudioClip SFX_OnIncorrect;
    public AudioClip SFX_VoiceOnCorrect;
    public AudioClip SFX_VoiceOnInCorrect;

    public string TXT_HACK_RESULT_PASS = "";
    public string TXT_HACK_RESULT_FAIL = "";

    void Awake() {
        i = this;
        base.Awake();
        SPNL_Result.rectTransform.localScale = new Vector3(0, 0, 1);
    }

    void Start () {
        LM.i.GetText("TXT_HACK_RESULT_PASS", "PASS", (STR) => { TXT_HACK_RESULT_PASS = STR; });
        LM.i.GetText("TXT_HACK_RESULT_FAIL", "FAIL", (STR) => { TXT_HACK_RESULT_FAIL = STR; });
        LM.i.FillLabel("LBL_Hacking", LBL_Hacking, "Hacking process...");
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetColor(Color C,Action OnFinish) {
        iTween.ColorTo(IMG_Background.gameObject, C, 0.3F,(NC)=> { IMG_Background.color = NC; },OnFinish);
    }

    public void FillPercent(float Percent = 100,Action OnFinish=null) {
        AM.i.PlaySFX(SFX_OnHack);
        iTween.ValueTo(gameObject, 0.0F, Percent / 100,1.5F,(V)=> {
            LBL_Percent.text = (((float)V)*100).ToString()+"%";
            IMG_ProgressBar.rectTransform.localScale = new Vector3((float)V,1,1);
        },OnFinish,"PERCENT");
    }

    public void ShowResult(bool Correct,Action OnFinish=null) {
        LBL_Percent.text = "0%";
        IMG_ProgressBar.rectTransform.localScale = new Vector3(0, 1, 1);
        Show(true, () => {
                FillPercent(Correct?100:(UnityEngine.Random.Range(50,80)), ()=> {
                    LBL_Result.text = Correct? TXT_HACK_RESULT_PASS : TXT_HACK_RESULT_FAIL;
                    LBL_Result.color = Correct ? Color.green:Color.red;
                    SPNL_Result.GetComponent<PanelBase>().Show(true,() => {
                        AM.i.PlaySFX(Correct ? SFX_OnCorrect : SFX_OnIncorrect);
                        GM.i.DelayAction(0.5F, () => { AM.i.PlaySFX(Correct ? SFX_VoiceOnCorrect: SFX_VoiceOnInCorrect); });
                        GM.i.DelayAction(2, () => {
                            SPNL_Result.GetComponent<PanelBase>().Show(false,
                                () => {
                                    Show(false, ()=> {
                                        OnFinish();
                                    });
                                });
                        });
                    });
                });
            });

    }  
}
