﻿using UnityEngine;
using System.Collections;
using System;
using GM = GlobalManager;
using QM = QuestionManager;
using AM = AudioManager;
using UnityEngine.UI;
using UnityEngine.EventSystems;
//using static QuestionManager;
using System.Linq;

public class QAOptionController : PanelBase {

    public Text TXT_A;
    public Image IMG_A;
    public Texture2D TEX_SFXQA;


    public AudioClip SFX_OnHover;
    public AudioClip SFX_OnHoverQA;

    public bool Correct = false;
    

    void Awake() {
        base.Awake();
        TXT_A = GetComponentInChildren<Text>(true);
        IMG_A = GetComponentsInChildren<Image>(true).Where(C=>C.name.Contains("IMG")).FirstOrDefault();
    }

	// Use this for initialization
	void Start () {
        TEX_SFXQA = Resources.Load<Sprite>("SoundWaveIcon").texture;

    }
	
	// Update is called once per frame
	void Update () {
	
	}

   public void Hover(bool active) {
       if (name.Contains("_Q")||blocked) return;
       if(SFX_OnHoverQA)
            AM.i.PlaySFX(SFX_OnHoverQA);
       else
            AM.i.PlaySFX(SFX_OnHover);
        iTween.StopByName("OPT_HOVER_" + GetInstanceID());
        GM.i.DelayFrameAction(3, () => {
            iTween.ValueTo(gameObject, RT.localScale.x, (active ? 1.25F : 1.00F), 0.25F, (V) => { RT.localScale = new Vector3((float)V, (float)V, 1); }, null, "OPT_HOVER_" + GetInstanceID());
        });
    }

    public void Show(bool show= true) {
        Debug.Log("QAOptionController -> Show");
        if (!show) {
            IMG_A.gameObject.SetActive(false);
            TXT_A.gameObject.SetActive(false);
        }
        base.Show(show);
    }

    public void Load(QM.Question Q,int Content = 0){
        SFX_OnHoverQA = null;
        if (Content < 0) Content = 0;
        if (Content > Q.Answers.Count) return;
        if (Content == 0) {
            switch (Q.QuestionType) {
                case QM.DataType.Text:
                    TXT_A.text = Q.value;
                    TXT_A.gameObject.SetActive(true);
                    break;
                case QM.DataType.Image:
                    IMG_A.sprite = Sprite.Create(Q.Media.Image, new Rect(0, 0, Q.Media.Image.width, Q.Media.Image.height), new Vector2(0.2F, 0.2F));
                    IMG_A.gameObject.SetActive(true);
                    break;
                case QM.DataType.Sound:
                    IMG_A.sprite = Sprite.Create(TEX_SFXQA, new Rect(0, 0, TEX_SFXQA.width, TEX_SFXQA.height), new Vector2(0.2F, 0.2F));
                    SFX_OnHoverQA = Q.Media.Audio;
                    break;
                case QM.DataType.Video:
                    break;

            }
        }
        else{
            Correct = Q.Answers[Content - 1].iscorrect;
            switch (Q.Answers[Content-1].AnswerType) {
                case QM.DataType.Text:
                    TXT_A.text = Q.Answers[Content - 1].value;
                    TXT_A.gameObject.SetActive(true);
                    break;
                case QM.DataType.Image:
                    IMG_A.sprite = Sprite.Create(Q.Answers[Content - 1].Media.Image, new Rect(0, 0, Q.Answers[Content - 1].Media.Image.width, Q.Answers[Content - 1].Media.Image.height), new Vector2(0.2F, 0.2F));
                    IMG_A.gameObject.SetActive(true);
                    break;
                case QM.DataType.Sound:
                    IMG_A.sprite = Sprite.Create(TEX_SFXQA, new Rect(0, 0, TEX_SFXQA.width, TEX_SFXQA.height), new Vector2(0.2F, 0.2F));
                    SFX_OnHoverQA = Q.Media.Audio;
                    break;
                case QM.DataType.Video:
                    break;
            }
       }
        Show();
    }

    public void UnLoad() {

    }

    public void Select() {
        QAPanelController.i.Answer(Correct);
    }

    override public void OnPointerClick() { blocked = true; Select(); }

    override public void OnPointerEnter() {Hover(true);}

    override public void OnPointerExit() { Hover(false); }
}

  

