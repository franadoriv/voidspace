﻿using UnityEngine;
using System.Collections;
using System;
using DPM = DancePadManager;
using MGL = MGL_MainGame;
using GMDS = MGL_MainGame.GameModes;

public class MIN_MainGame : MainInputManager {

    public static MIN_MainGame i;

    public GameObject Player;
    public GameObject PlayerShip;
    Vector3 KeyDirection;

    public bool MouseLeftDown = false;

    public void CheckInputs() {
        switch (MGL.i.CurrentGameMode) {
            case GMDS.Cutscene: break;
            case GMDS.Evasion: CheckEvasionInput(); break;
            case GMDS.Attacking: CheckAtackInput(); break;
        }
    }

    public void CheckEvasionInput() {
        if (MGL.i.CurrentGameMode != GMDS.Evasion) return;
        float x = Player.transform.localPosition.x;
        float y = Player.transform.localPosition.y;
        KeyDirection = new Vector3(
            ((Input.GetKey(KeyCode.LeftArrow)&&x>-4.0F) ? 0.1F : 0.0F) - ((Input.GetKey(KeyCode.RightArrow)&&x<4.0F) ? 0.1F : 0.0F),
            ((Input.GetKey(KeyCode.UpArrow  )&&y<2.5F) ? 0.1F : 0.0F) - ((Input.GetKey(KeyCode.DownArrow)&&y>-2.5F) ? 0.1F : 0.0F), 
            0);
        Player.transform.Translate(KeyDirection);
        if (Input.GetMouseButtonDown(0))
            PlayerHelper.i.Shoot();
        if(PlayerShip!=null)
        PlayerShip.transform.Rotate(new Vector3(
                                                (Input.GetKey(KeyCode.DownArrow) ? 5F : 0) - (Input.GetKey(KeyCode.UpArrow) ? 5F : 0),
                                                0,
                                                (Input.GetKey(KeyCode.LeftArrow) ? 5F : 0) - (Input.GetKey(KeyCode.RightArrow) ? 5F : 0)
                                                ));

    }

    public void CheckAtackInput() {
        
        if (DPM.Detected) { 
        
        }
        else {
            float x = Player.transform.localPosition.x;
            float y = Player.transform.localPosition.y;
            KeyDirection = new Vector3(
                ((Input.GetKey(KeyCode.LeftArrow) && x > -4.0F) ? 0.1F : 0.0F) - ((Input.GetKey(KeyCode.RightArrow) && x < 4.0F) ? 0.1F : 0.0F),
                ((Input.GetKey(KeyCode.UpArrow) && y < 2.5F) ? 0.1F : 0.0F) - ((Input.GetKey(KeyCode.DownArrow) && y > -2.5F) ? 0.1F : 0.0F),
                0);
            if (MGL.i.CurrentGameMode == GMDS.Evasion)
                Player.transform.Translate(KeyDirection);
            if (MouseLeftDown && MGL.i.CurrentGameMode == GMDS.Attacking)
                PlayerHelper.i.Shoot();
        }
    }


    void Awake() {
        //base.Awake();
        i = this;
    }


	// Use this for initialization
	void Start () {
	
	}
    public float HT = 0.0F;
	// Update is called once per frame
	void Update () {
        if (Input.anyKey && InputEnabled) CheckInputs();

        if (Input.GetMouseButtonDown(0))
            MouseLeftDown = true;
        if (Input.GetMouseButtonUp(0))
            MouseLeftDown = false;
	}
}
