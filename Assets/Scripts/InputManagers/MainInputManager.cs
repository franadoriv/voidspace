﻿using UnityEngine;
using System.Collections;
using System;

public class MainInputManager : MonoBehaviour {

    public static MainInputManager i;

    //Managers
    public DancePadManager DPM;

    //Variables
    public bool InputEnabled = false;
    public bool GameInputEnabled = false;
    public bool GamePadDetected = true;

    public void WriteOnDebug(string Message) {
        Debug.Log(Message);
    }

    public void Prepare() {
        DPM = GameObject.Find("DancePadManager").GetComponent<DancePadManager>();
    }

    void Awake() {
        MainInputManager.i = this;
    }

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
