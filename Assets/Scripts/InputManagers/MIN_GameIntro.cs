﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using MSC = MSC_GameIntro;

public class MIN_GameIntro : MainInputManager {

    Image IMG_FadeScreen;
    public bool Skipeable = false;

    IEnumerator SkipeableTimeOut(int Seconds) {
        yield return new WaitForSeconds(Seconds);
        Skipeable = true;
    }

	// Use this for initialization
	void Start () {
        StartCoroutine(SkipeableTimeOut(10));
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.anyKeyDown && Skipeable) {
           MSC.i.FinishIntro();
        }
	
	}
}
